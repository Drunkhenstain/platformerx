﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBackground : MonoBehaviour
{


    public float ParalaxFactor;
    public float speed;

    private float x;

    public float PontoDeDestino;

    private float OriginX;
    private float OriginY;
    private float OriginZ;

    public Transform BackgroundGroup;

    public Vector3 LastCameraPos;

    private GameObject _cam;
    private Transform _cameraPos;

    public float CameraOriginX;

    Vector3 CamToGroupOffset;

    private Vector3 theDimension;

    private float yModifier = 0.1f;


    // Use this for initialization
    void Start()
    {
        OriginX = transform.position.x;
        OriginY = transform.position.y;
        OriginZ = transform.position.z;

        theDimension = GetComponent<Renderer>().bounds.size;

        _cam= GameObject.FindGameObjectWithTag("MainCamera");

        if (_cam)
        {
            _cameraPos = _cam.transform;
            CameraOriginX = _cameraPos.transform.position.x;
            CamToGroupOffset = _cameraPos.transform.position + BackgroundGroup.transform.position;
        }
        else
        {
        }

        LastCameraPos = new Vector3(_cameraPos.position.x, _cameraPos.position.y, _cameraPos.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        BackgroundGroup.transform.position = new Vector3(_cameraPos.position.x + CamToGroupOffset.x, BackgroundGroup.transform.position.y, BackgroundGroup.transform.position.z); ;

        Vector3 newPos = _cameraPos.position * (1 - ParalaxFactor);

        newPos.z = OriginZ;
        newPos.y = OriginY;
        newPos.x += OriginX;

        transform.position = newPos;

        if (_cameraPos.position.x > (transform.position.x + theDimension.x))
        {
            transform.position = new Vector3(transform.position.x + theDimension.x + theDimension.x + theDimension.x, transform.position.y, transform.position.z);
        }
        if (_cameraPos.position.x < (transform.position.x - theDimension.x))
        {
            transform.position = new Vector3(transform.position.x - (theDimension.x + theDimension.x + theDimension.x), transform.position.y, transform.position.z);
        }
    }
}
