﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    public class CheckPointController : MonoBehaviour
    {
        public int CheckPointIndex;
        public int CheckPointLevelIndex;
        public bool IsFinish;

        [ColorUsage(true, true)]
        public Color ActiveGlowColor;
        [ColorUsage(true, true)]
        public Color InactiveGlowColor;
        [ColorUsage(true, true)]
        public Color NotFoundGlowColor;

        private bool _checkpointFound;
        private GlowController _glowEffect;

        private void Start()
        {
            _glowEffect = GetComponent<GlowController>();
            _checkpointFound = false;

            _glowEffect.SetGlow(0.99f, NotFoundGlowColor);
        }

        public Vector3 GetPosition()
        {
            return gameObject.transform.position;
        }

        public void SetActive()
        {
            if (_checkpointFound == false)
            {
                _checkpointFound = true;
            }

            _glowEffect.SetGlow(0.99f, ActiveGlowColor);
        }
        public void SetInactive()
        {
            _glowEffect.SetGlow(0.99f, InactiveGlowColor);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (StaticChecks.IsPlayer(other.gameObject))
            {
                StaticChecks.GetPlayer(other.gameObject).OnCheckPointContact(this);
            } 
        }
    }
}