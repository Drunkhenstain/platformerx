﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    [CreateAssetMenu(fileName = "PlayerAnimationSettings_V0", menuName = "ScriptableObjects/PlayerAnimation", order = 2)]
    public class PlayerAnimationSettings : ScriptableObject
    {
        [Header("Effects")]
        [ColorUsage(true, true)]
        public Color DissolveColor;

        [ColorUsage(true, true)]
        public Color GlowColorDash;


        public float OnMeleeHitSloMoLength;
        public float OnMeleeHitSloMoAmount;
    }
}
