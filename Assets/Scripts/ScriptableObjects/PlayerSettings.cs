﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    [CreateAssetMenu(fileName = "PlayerSettings_V0", menuName = "ScriptableObjects/Player", order = 1)]
    public class PlayerSettings : ScriptableObject
    {
        [Header("Activate")]
        public bool UseSettings;
        
        [Header("Attributes")]
        public float BaseHealth;
        public float BaseEnergy;
        public float EnergyLoadingRate;

        [Header("Timers")]
        [Range(0f, 20)]
        public float ImmunityAfterHitTime;

        [Range(0f, 20)]
        public float HurtStateLength;

        [Range(0f, 20)]
        public float StunStateLength;

        [Range(0f, 20)]
        public float MeeleeAttackLength;
        [Range(0f, 20)]
        public float CantAttackAfterMeeleeAttackLength;

        [Range(0.1f, 20)]
        public float BaseMoveSpeed;
        [Range(0.1f, 40)]
        public float MaxSlipOnFloorSpeed;
        [Range(0f, 100f)]
        public float MaxFallingSpeed;

        [Header("Ground Movement")]
        [Range(0f, 5)]
        public float GravityScaleOnGround;

        [Range(0.01f, 5)]
        public float GroundSpeedModifierOnStop;
        [Range(0.01f, 5)]
        public float GroundSpeedModifierOnTurn;
        [Range(0.01f, 5)]
        public float GroundSpeedModifierOnAccelerate;

        [Header("Air Movement")]
        [Range(0.01f, 5)]
        public float AirSpeedModifierOnStop;
        [Range(0.01f, 5)]
        public float AirSpeedModifierOnTurn;
        [Range(0.01f, 5)]
        public float AirSpeedModifierOnAccelerate;
        
        [Header("Dashing")]
        [Range(0.1f, 20)]
        public float DashForceX;
        [Range(0.1f, 20)]
        public float DashForceY;
        [Range(0f, 20)]
        public float DashHangTime;
        [Range(0f, 20)]
        public float MaxDashTime;
        [Range(0f, 5)]
        public float DashGravityScale;        

        [Header("Jumping")]
        [Range(0.1f, 20)]
        public float JumpForce;
        [Range(0, 2)]
        public float WallJumpXForceModifier;
        [Range(0.0001f, 1)]
        public float AirDampingOnTurnAfterWallJump;
        [Range(0f, 1)]
        public float JumpBreakFactor;
        [Range(0f, 10)]
        public float JumpCoyoteTime;

        [Header("Wall Sliding")]
        [Range(0.1f, 20)]
        public float WallSlidingSpeed;
        [Range(0.1f, 20)]
        public float SlipperyWallSlidingSpeed;

        [Header("General")]
        [Range(0f, 20)]
        public float GravityScale;
        [Range(0f, 20)]
        public float Mass;

        [Header("Dont Touch!")]
        public float GroundedDistanceCheck;
        [Range(0f, 2)]
        public float LedgeDistanceCheck;
        [Range(0f, 2)]
        public float WallDistanceCheck;

        public float JumpWaitForSuccessTime;
    }
}