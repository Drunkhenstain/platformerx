﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace PlatformerX
{
    public class DoorTriggerPoints : BaseDoorTrigger
    {
        public int PointsNeeded;

        private GlobalBlackboard _blackbord;

        private void Start()
        {
            OnStart();
            _blackbord = FindObjectOfType<GlobalBlackboard>();
            _door.Text.SetText(PointsNeeded.ToString() + "\nPts", true);
        }

        protected override void OnPlayerInRange()
        {
            if (_door.IsOpen == false && _blackbord.CurrentPointsNoDeath >= PointsNeeded)
            {
                _door.OpenDoor();
                _door.Text.SetText(":D", true);
            }
        }

        protected override void OnPlayerOutOfRange()
        {

        }


        private void OnValidate()
        {
            if (_door&& _door.Text)
            {
                _door.Text.SetText(PointsNeeded.ToString() + "\nPts", true);
            }
        }

    }
}