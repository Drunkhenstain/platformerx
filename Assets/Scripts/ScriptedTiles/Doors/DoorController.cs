﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace PlatformerX
{
    [RequireComponent(typeof(Animator))]
    public class DoorController : MonoBehaviour
    {
        public TextMeshPro Text;

        private Animator _anim;

        public bool IsOpen;

        // Start is called before the first frame update
        void Start()
        {
            _anim = GetComponent<Animator>();
            if (Text == null)
            {
                GetComponentInChildren<TextMeshPro>();
            }

            IsOpen = false;
        }

        public void OpenDoor()
        {
            if (IsOpen == false)
            {
                Debug.Log("OpenDoor");
                _anim.Play("Open");
                IsOpen = true;
            }
        }
    }
}