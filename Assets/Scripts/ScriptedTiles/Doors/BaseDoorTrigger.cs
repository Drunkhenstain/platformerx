﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    [RequireComponent(typeof(Collider2D))]

    public abstract class BaseDoorTrigger : MonoBehaviour
    {
        [SerializeField]
        protected DoorController _door;

        // Start is called before the first frame update
        protected void OnStart()
        {
            //_door = GetComponent<DoorController>(); 
        }


        private void OnTriggerEnter2D(Collider2D collision)
        {
            PlayerStateController player = StaticChecks.GetPlayer(collision.gameObject);

            if (player != null)
            {
                OnPlayerInRange();
            }
        }
        private void OnTriggerExit2D(Collider2D collision)
        {
            PlayerStateController player = StaticChecks.GetPlayer(collision.gameObject);

            if (player != null)
            {
                OnPlayerOutOfRange();
            }
        }
         
        protected abstract void OnPlayerInRange();
        protected abstract void OnPlayerOutOfRange();

    }
}