﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(GlowController))]
    public class AxeTrapController : MonoBehaviour
    {
        public bool Active = true;
        public bool StartLeft = true;

        public float IdleTime = 1;
        public float SwingTime = 0.8f;

        [ColorUsage(true, true)]
        public Color DissolveColor;

        public Transform AxeLengthBone;

        private Animator _anim;
        private GlowController _glowEffect;

        private const string IdleLengthName = "IdleLength";
        private const string SwingLengthName = "SwingLength";

        // Start is called before the first frame update
        void Start()
        {
            _anim = GetComponent<Animator>();
            _glowEffect = GetComponent<GlowController>();

            if (StartLeft)
            {
                _anim.Play("IdleLeft");
            }
            else
            {
                _anim.Play("IdleRight");
            }

            if (Active)
            {
                _anim.SetFloat(IdleLengthName, 1 / IdleTime);
                _anim.SetFloat(SwingLengthName, 1 / SwingTime);
            }
            else
            {
                _anim.speed = 0;
            }
        }

        public void StartGlow()
        {
            _glowEffect.StartEffect(IdleTime - (IdleTime / 9), 0.9f, DissolveColor);
        }

        public void ResetGlow()
        {
            _glowEffect.ResetSprites(DissolveColor);
        }

        // Update is called once per frame
        void Update()
        {
            if (Active)
            {
                if (_anim.speed != 1)
                {
                    _anim.speed = 1;
                }
            }
            else
            {
                if (_anim.speed != 0)
                {
                    _anim.speed = 0;
                }
                _glowEffect.ResetSprites(DissolveColor);
            }
        }

        private void OnValidate()
        {
            if (!_anim)
            {
                _anim = GetComponent<Animator>();
            }

            if (StartLeft)
            {
                _anim.Play("IdleLeft");
            }
            else
            {
                _anim.Play("IdleRight");
            }
        }
    }
}
