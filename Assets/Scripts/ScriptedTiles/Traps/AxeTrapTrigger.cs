﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    [RequireComponent(typeof(Collider2D))]
    public class AxeTrapTrigger : MonoBehaviour
    {
        Collider2D _collider;
        
        private PlayerStateController _tempPlayer;
        public PlayerStateController Player;

        void Start()
        {
            _collider = GetComponent<Collider2D>();
        }

        public void OnCollisionEnter2D(Collision2D other)
        {
            _tempPlayer = StaticChecks.GetPlayer(other.gameObject);

            if (_tempPlayer != null)
            {
                Player = _tempPlayer;
                Player.OnAxeTrapContact(other.GetContact(0).point + Vector2.zero, this.transform);
                _collider.enabled = false;
            }
                      
        }
    }
}
