﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace PlatformerX
{
    //[RequireComponent(typeof(TilemapCollider2D))]
    public class DeadlyFloorController : MonoBehaviour
    {
        public void OnCollisionEnter2D(Collision2D other)
        {
            PlayerStateController player = StaticChecks.GetPlayer(other.gameObject);
              
            if (player != null)
            {
                player.OnDeadlyFloorContact(other.GetContact(0).point);
            }
            else
            {
                //Debug.Log("No Player found");
            }
        }
    }
}
