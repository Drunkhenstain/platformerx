﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    [RequireComponent(typeof(Animator))]
    public class UpgradeStationController : MonoBehaviour
    {
        public float HeathRecoverTime;
        private float _curRecoverTime;

        private bool _skillCollectedByPlayer;
        private bool _canGetHealth;
        public float GetHealthOnEnter;

        public bool EnablesWallJump;
        public bool EnablesWallSlide;
        public bool EnablesLedgeClimb;
        public bool EnablesAxeAttack;
        public bool EnablesDash;

        private Animator _anim;

        // Start is called before the first frame update
        void Start()
        {
            _anim = GetComponent<Animator>();

            if (GetHealthOnEnter > 0)
            {
                _canGetHealth = true;
            }
        }

        private void Update()
        {
            if (_canGetHealth == false)
            {
                _curRecoverTime += Time.deltaTime;

                if (_curRecoverTime >= HeathRecoverTime)
                {
                    _canGetHealth = true;
                    _curRecoverTime = 0;
                }
            }
        }

        // Update is called once per frame
        private void OnTriggerEnter2D(Collider2D collision)
        {
            var player = StaticChecks.GetPlayer(collision.gameObject);

            if (player)
            {
                if (_canGetHealth && GetHealthOnEnter > 0)
                {
                    player.GetHealth(GetHealthOnEnter);
                    _canGetHealth = false;

                    if (_anim)
                    {
                        _anim.Play("Collect");
                    }
                }

                if (_skillCollectedByPlayer == false)
                {
                    if (EnablesAxeAttack)
                    {
                        player.EnableStateMeleeAttack();
                    }
                    if (EnablesDash)
                    {
                        player.EnableStateDash();
                    }
                    if (EnablesLedgeClimb)
                    {
                        player.EnableStateLedgeClimb();
                    }
                    if (EnablesWallJump)
                    {
                        player.EnableStateWallJump();
                    }
                    if (EnablesWallSlide)
                    {
                        player.EnableStateWallSlide();
                    }

                    _anim.Play("Collect");
                    _skillCollectedByPlayer = true;
                }
            }
        }
    }
}