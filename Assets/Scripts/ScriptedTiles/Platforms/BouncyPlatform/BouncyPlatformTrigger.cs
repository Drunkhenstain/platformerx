﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    public class BouncyPlatformTrigger : MonoBehaviour
    {
        [SerializeField]
        private BouncyPlatformController _controller;

        public void OnCollisionEnter2D(Collision2D other)
        {
            _controller.DoBounce(other);
        }
    }
}