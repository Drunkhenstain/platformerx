﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    [RequireComponent(typeof(Animator))]
    public class BouncyPlatformController : MonoBehaviour
    {
        public float JumpForce = 20f;
        public float AnimLength = 0.3f;

        private Animator _anim;

        private bool _canBounce;

        private void Start()
        {
            _anim = GetComponent<Animator>();
            _canBounce = true;
        }

        public void DoBounce(Collision2D collision)
        {
            _anim.Play("Bounce");
            _anim.speed = 1 / AnimLength;

            PlayerStateController player = StaticChecks.GetPlayer(collision.gameObject);

            if (player != null)
            {
                player.OnBouncePlatformContact(JumpForce);
            }
        }

        public void BounceFinishEvent()
        {
            _canBounce = true;
            _anim.speed = 1;
        }
    }
}
