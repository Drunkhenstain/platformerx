﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class MovingPlatform : MonoBehaviour
    {
        public bool TransportObjects = true;
        public bool GoLastToFirstWaypoint = true;
        public float Speed;
        public float WaitOnWaypointTime;
        public List<Transform> Waypoints;

        private Transform GotoWaypoint;

        private Rigidbody2D _rigid;
        private PlayerStateController _player;

        private bool _wait;

        private int _curWaypointIndex;
        private float _curWaitOnWaypointTime;

        void Start()
        {
            _curWaitOnWaypointTime = WaitOnWaypointTime;
            _curWaypointIndex = 0;
            _rigid = GetComponent<Rigidbody2D>();

            if (Waypoints != null && Waypoints.Count > 0)
            {
                GotoWaypoint = Waypoints[_curWaypointIndex];
            }
        }

        void FixedUpdate()
        {
            if (GotoWaypoint)
            {
                // is on waypoint
                if (Vector2.Distance(transform.position, GotoWaypoint.position) < 0.1f && _wait == false)
                {
                    _wait = true;

                    if (_curWaypointIndex + 1 < Waypoints.Count)
                    {
                        _curWaypointIndex += 1;
                    }
                    else
                    {
                        _curWaypointIndex = 0;

                        if (GoLastToFirstWaypoint == false)
                        {
                            Waypoints.Reverse();
                        }
                    }
                    GotoWaypoint = Waypoints[_curWaypointIndex];
                }

                if (_wait)
                {
                    _curWaitOnWaypointTime -= Time.fixedDeltaTime;

                    if (_curWaitOnWaypointTime <= 0)
                    {
                        _wait = false;
                        _curWaitOnWaypointTime = WaitOnWaypointTime;
                    }
                }

                if (_wait == false)
                {
                    transform.position = Vector3.MoveTowards(transform.position, GotoWaypoint.transform.position, Speed * Time.fixedDeltaTime);
                }
            }
        }
        public void OnCollisionEnter2D(Collision2D other)
        {
            if (TransportObjects)
            {
                if (_player == null)
                {
                    _player = StaticChecks.GetPlayer(other.gameObject);
                }
                if (_player != null)
                {
                    Debug.Log("MovingPlatform :Player SetParent");
                    _player.transform.SetParent(transform);
                }
                else
                {
                    Debug.Log("MovingPlatform :No Player found");
                }
            }
        }


        public void OnCollisionExit2D(Collision2D other)
        {
            if (_player == null)
            {
                _player = StaticChecks.GetPlayer(other.gameObject);
            }

            if (_player != null)
            {
                Debug.Log("MovingPlatform :Player Unparent");
                _player.transform.parent = null;
            }
            else
            {
                Debug.Log("MovingPlatform :No Player found");
            }
        }
    }
}