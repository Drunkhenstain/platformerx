﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParalaxController : MonoBehaviour
{
    [Range(0,1)]
    public float yModifier;
    [Range(0, 1)]
    public float parallaxFactor;
    public Transform cameraPos;


    [SerializeField]
    private Vector3 theStartPosition;

    void Start()
    {


        theStartPosition = transform.position;
    }

    void Update()
    {
        Vector3 newPos = cameraPos.position * parallaxFactor;                   // Calculate the position of the object
        
        newPos.z = 0;
        newPos.x += theStartPosition.x;
        newPos.y += theStartPosition.y;
        newPos.y *= yModifier;
        transform.position = newPos;
    }
}
