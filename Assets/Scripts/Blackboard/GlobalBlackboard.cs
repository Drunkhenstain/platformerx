﻿using PlatformerX;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalBlackboard : MonoBehaviour
{
    public float PlayerKillsTotal;

    public float MaxKillsNoDeath;
    public float CurrentKillsNoDeath;

    public float MaxPointsNoDeath;
    public float CurrentPointsNoDeath;

    public PlayerStateController Player;

    void Start()
    {
        Player = FindObjectOfType<PlayerStateController>();
    }

    public void PlayerDied()
    {
        CheckMaxValues();

        if (CurrentPointsNoDeath > 1)
        {
            CurrentPointsNoDeath = Mathf.RoundToInt(CurrentPointsNoDeath / 2);
        }
        CurrentKillsNoDeath = 0;
    }

    public void EnemyDied(float points)
    {
        Debug.Log("Enemy Killed");
        PlayerKillsTotal += 1;

        CurrentPointsNoDeath += points;

        CurrentKillsNoDeath += 1;

        CheckMaxValues();
    }

    private void CheckMaxValues()
    {
        if (CurrentKillsNoDeath > MaxKillsNoDeath)
        {
            MaxKillsNoDeath = CurrentKillsNoDeath;
        }
        if (CurrentPointsNoDeath > MaxPointsNoDeath)
        {
            MaxPointsNoDeath = CurrentPointsNoDeath;
        }
    }
}
