﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlowController : MonoBehaviour
{
    public Material GlowMaterial;

    public SpriteRenderer[] Sprites;

    public bool OverrideEffectOnStart = false;

    [ColorUsage(true, true)]
    public Color OverrideGlowColor;


    private bool _isDissolving;
    private float _maxThickness;

    private float _curDissolveAmount;
    private float _duration;

    const string _ModifierName = "GlowModifier";
    const string _ColorName = "GlowColor";

    private Color OriginalGlowColor;

    private void Start()
    {
        _maxThickness = 0.99f;
        //if (Sprites != null)
        //{
        //    bool getColor = true;
        //    foreach (var sprite in Sprites)
        //    {
        //        if (getColor)
        //        {
        //            OriginalGlowColor = sprite.material.GetColor(_ColorName);
        //            getColor = false;
        //        }
        //    }
        //}
        if (OverrideEffectOnStart)
        {
            SetGlow(0.99f, OverrideGlowColor);
        }
    }

    void Update()
    {
        if (_isDissolving)
        {
            if (Sprites != null)
            {
                float curMatVal = 0;
                foreach (var sprite in Sprites)
                {
                    curMatVal = sprite.material.GetFloat(_ModifierName);

                    if (curMatVal + (_curDissolveAmount * Time.deltaTime) <= _maxThickness)
                    {
                        sprite.material.SetFloat(_ModifierName, curMatVal + (_curDissolveAmount * Time.deltaTime));
                    }
                }
                _duration -= Time.deltaTime;
            }

            if (_duration <= 0)
            {
                _isDissolving = false;
            }
        }
    }

    public void StartEffect(float length, float maxThickness, Color colorHDR)
    {
        ResetSprites(colorHDR, true);

        _isDissolving = true;
        _duration = length;
        _maxThickness = maxThickness >= 1 ? 0.99f : maxThickness < 0 ? 0 : maxThickness;

        _curDissolveAmount = 1 / length;
    }

    public void ResetSprites(Color colorHDR, bool visible = true)
    {
        SetMaterial();
        _isDissolving = false;
        if (Sprites != null)
        {
            foreach (var sprite in Sprites)
            {
                sprite.material.SetFloat(_ModifierName, visible ? 0 : 1);
                sprite.material.SetColor(_ColorName, colorHDR);
            }
        }
    }

    public void SetGlow(float maxThickness, Color colorHDR)
    {
        SetMaterial();
        _isDissolving = false;
        _maxThickness = maxThickness;

        if (Sprites != null)
        {
            foreach (var sprite in Sprites)
            {
                sprite.material.SetFloat(_ModifierName, _maxThickness);
                sprite.material.SetColor(_ColorName, colorHDR);
            }
        }
    }

    private void SetMaterial()
    {
        if (Sprites != null)
        {
            foreach (var sprite in Sprites)
            {
                if (sprite.material.name != "")
                {
                    sprite.material = GlowMaterial;
                }
            }
        }
    }

}
