﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    public class DissolveController : MonoBehaviour
    {
        public Material DissolveMaterial;
        public SpriteRenderer[] Sprites;

        private float DissolveAmount;
        private float Duration;
        private bool IsDissolving;
        const string _ModifierName = "DissolveModifier";
        const string _ColorName = "DissolveColor";
         
        private Color OriginalDissolveColor;

        private void Start()
        {
            //if (Sprites != null)
            //{
            //    bool getColor = true;
            //    foreach (var sprite in Sprites)
            //    {
            //        if (getColor)
            //        {
            //            OriginalDissolveColor = sprite.material.GetColor(_ColorName);
            //            getColor = false;
            //        }
            //    }
            //}
        }

        void Update()
        {
            if (IsDissolving)
            {
                if (Sprites != null)
                {
                    float curMatVal = 0;
                    foreach (var sprite in Sprites)
                    {
                        curMatVal = sprite.material.GetFloat(_ModifierName);

                        if (curMatVal > 0)
                        {
                            sprite.material.SetFloat(_ModifierName, curMatVal - (DissolveAmount * Time.deltaTime));
                        }
                    }
                    Duration -= Time.deltaTime;
                }

                if (Duration <= 0)
                {
                    IsDissolving = false;
                }
            }
        }

        public void ResetSprites(Color colorHDR, bool visible = true)
        {
            SetMaterial();
            if (Sprites != null)
            {
                foreach (var sprite in Sprites)
                {
                    sprite.material.SetFloat(_ModifierName, visible ? 1 : 0);
                    sprite.material.SetColor(_ColorName, colorHDR);
                }
            }
        }

        public void SetDissolve(float amount, Color colorHDR)
        {
            SetMaterial();
            if (Sprites != null)
            {
                foreach (var sprite in Sprites)
                {
                    if (sprite.material.name != "")
                    {
                        sprite.material.SetFloat(_ModifierName, amount > 1 ? 1 : amount < 0 ? 0 : amount);
                        sprite.material.SetColor(_ColorName, colorHDR);
                    }
                }
            }
        }

        public void StartDissolve(float length, Color colorHDR)
        {
            ResetSprites(colorHDR, true);

            IsDissolving = true;
            Duration = length;

            DissolveAmount = 1 / length;
        }

        private void SetMaterial()
        {
            if (Sprites != null)
            {
                foreach (var sprite in Sprites)
                {
                    if (sprite.material.name != "")
                    {
                        sprite.material = DissolveMaterial;
                    }
                }
            }
        }
    }
}