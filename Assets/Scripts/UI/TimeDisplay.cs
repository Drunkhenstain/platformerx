﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PlatformerX
{
    public class TimeDisplay : MonoBehaviour
    {
        private Text _text;
        private const string _displayString = "Time {0:n0}";

        void Start()
        {

            _text = GetComponentInChildren<Text>();
        }

        void LateUpdate()
        {
            _text.text = string.Format(_displayString, Time.time);
        }
    }
}