﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PlatformerX
{
    public class BlackboardDisplay : MonoBehaviour
    {
        public bool ShowCurrentKills;
        public bool ShowTotalKills;
        public bool ShowPoints;
        public bool ShowMaxPoints;
        public bool ShowMaxKills;

        private GlobalBlackboard _blackboard;
        private Text _text;

        private const string _displayStringKills = "Kills {0:n0}";

        private const string _displayStringTotalKills = "TotalKills {0:n0}";

        private const string _displayStringPoints = "Points {0:n0}";

        private const string _displayStringMaxPoints = "Max Points {0:n0}";

        private const string _displayStringMaxKills =  "Max Kills {0:n0}";


        void Start()
        {
            _blackboard = FindObjectOfType<GlobalBlackboard>();
            _text = GetComponentInChildren<Text>();
        }

        void LateUpdate()
        {
            if (ShowCurrentKills)
            {
                _text.text = string.Format(_displayStringKills, _blackboard.CurrentKillsNoDeath);
            }
            else if (ShowTotalKills)
            {
                _text.text = string.Format(_displayStringTotalKills, _blackboard.PlayerKillsTotal);
            }
            else if (ShowPoints)
            {
                _text.text = string.Format(_displayStringPoints, _blackboard.CurrentPointsNoDeath);
            }
            else if (ShowMaxPoints)
            {
                _text.text = string.Format(_displayStringMaxPoints, _blackboard.MaxPointsNoDeath);
            }
            else if (ShowMaxKills)
            {
                _text.text = string.Format(_displayStringMaxKills, _blackboard.MaxKillsNoDeath);
            }
        }
    }
}