﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PlatformerX
{
    [RequireComponent(typeof(Slider))]
    public class HealthbarUISlider : MonoBehaviour
    {
        private PlayerComponents _plParts;
        private Slider _slider;

        private float _min = 0;
        private float _max = 1;
        private bool _tryGetPlParts = false;

        // Start is called before the first frame update
        void Start()
        {
            _slider = GetComponent<Slider>();
            _plParts = FindObjectOfType<PlayerComponents>();

            if (_plParts == null)
            {
                _tryGetPlParts = true;
            }
            else
            {
            }
        }

        void LateUpdate()
        {
            if (_tryGetPlParts && _plParts == null)
            {
                _plParts = FindObjectOfType<PlayerComponents>();
                _tryGetPlParts = false;
            }

            if (_plParts != null)
            {
                UpdateValue();
            }
        }

        private void UpdateValue()
        {
            _slider.minValue = 0;
            _slider.maxValue = _plParts.Settings.BaseHealth;
            _slider.value = _plParts.PlayerStatus.CurrentHealth < 0.001f ? 0.001f : _plParts.PlayerStatus.CurrentHealth;
        }
    }
}
