﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PlatformerX
{
    public class DeathCounter : MonoBehaviour
    {
        private PlayerStatus _plStatus;
        private Text _text;
        private int _deaths;
        private bool _tryGetPlParts = false;

        private const string _displayString = "Deaths {0}";

        // Start is called before the first frame update
        void Start()
        {
            _deaths = 0;
            _plStatus = FindObjectOfType<PlayerStatus>();
            _text = GetComponentInChildren<Text>();

            _text.text = string.Format(_displayString, _deaths);

            if (_plStatus == null)
            {
                _tryGetPlParts = true;
            }
        }

        // Update is called once per frame
        void LateUpdate()
        {
            if (_tryGetPlParts && _plStatus == null)
            {
                _plStatus = FindObjectOfType<PlayerStatus>();
                _tryGetPlParts = false;

                if (_plStatus)
                {
                    _deaths = _plStatus.DeathCount;
                }
            }

            if (_plStatus != null)
            {
                UpdateValue();
            }
        }

        private void UpdateValue()
        {
            if (_deaths != _plStatus.DeathCount)
            {
                _deaths = _plStatus.DeathCount;
                _text.text = string.Format(_displayString, _deaths);
            }
        }
    }
}