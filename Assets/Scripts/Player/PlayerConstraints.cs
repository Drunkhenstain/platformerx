﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    [RequireComponent(typeof(PlayerStatus))]
    [RequireComponent(typeof(PlayerComponents))]
    public class PlayerConstraints : MonoBehaviour
    {
        public float maxXVel;
        public float maxYVel;

        private PlayerStatus _plStatus;
        private PlayerComponents _plParts;

        // Start is called before the first frame update
        void Start()
        {
            _plStatus = GetComponent<PlayerStatus>();
            _plParts = GetComponent<PlayerComponents>();
        }

        // Update is called once per frame
        //void FixedUpdate()
        //{
        //    if (_plParts.Rigid.velocity.x > maxXVel)
        //    {
        //        _plParts.Rigid.velocity = new Vector2(maxXVel, _plParts.Rigid.velocity.y);
        //    }
        //    else if (_plParts.Rigid.velocity.x < -maxXVel)
        //    {
        //        _plParts.Rigid.velocity = new Vector2(-maxXVel, _plParts.Rigid.velocity.y);
        //    }


        //    if (_plParts.Rigid.velocity.y > maxYVel)
        //    {
        //        _plParts.Rigid.velocity = new Vector2(_plParts.Rigid.velocity.x, maxYVel);
        //    }
        //    else if (_plParts.Rigid.velocity.y < -maxYVel)
        //    {
        //        _plParts.Rigid.velocity = new Vector2(_plParts.Rigid.velocity.x, -maxYVel);
        //    }
        //}
    }
}