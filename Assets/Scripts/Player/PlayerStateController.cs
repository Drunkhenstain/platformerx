﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace PlatformerX
{
    [RequireComponent(typeof(PlayerInput))]
    [RequireComponent(typeof(PlayerComponents))]
    [RequireComponent(typeof(PlayerStatus))]
    public class PlayerStateController : StateMachine
    {
        private Vector3 _startPositon;
        private CheckPointController _activeCheckPoint;
        private List<CheckPointController> AllCheckPoints;
        private List<CheckPointController> LevelCheckPoints;
        public Vector3 GetActiveCheckpointPosition
        {
            get
            {
                return _activeCheckPoint != null ? _activeCheckPoint.transform.position : _startPositon;
            }
        }

        [Range(0, 20)]
        public int EditorSwapCheckpointIndex;
        private int LastEditorSwapCheckpointIndex;

        [Range(0, 3)]
        public int CurrentLevelIndex;


        public PlayerInput _input;
        public PlayerComponents _playerParts;
        public PlayerStatus _plStatus;

        #region Unity
        private void Update()
        {
            if (_playerParts && _playerParts.CamLookAt)
            {
                _playerParts.CamLookAt.position = transform.position + new Vector3(_input.LookInputX, _input.LookInputY) * 4f;
            }

            // check dead
            if (_plStatus.CurrentHealth <= 0)
            {
                if (currentState is PlayerStateReset == false)
                {
                    SetState<PlayerStateHurt>();
                }
            }

            // set dissolve according to health
            if (currentState is PlayerStateDash == false && currentState is PlayerStateHurt == false && currentState is PlayerStateReset == false)
            {
                if (lastState is PlayerStateDash == false && currentState is PlayerStateAxeAttack == false)
                {
                    _plStatus.SetHealthDissolve();
                }
            }
        }
        // on editor change values for debug/testing
        void OnValidate()
        {
            if (LastEditorSwapCheckpointIndex != EditorSwapCheckpointIndex && EditorSwapCheckpointIndex < AllCheckPoints.Count && AllCheckPoints.IndexOf(_activeCheckPoint) >= 0)
            {
                LastEditorSwapCheckpointIndex = EditorSwapCheckpointIndex;

                _activeCheckPoint = AllCheckPoints[EditorSwapCheckpointIndex];

                if (PlayerBaseState.CheckEntryConditions<PlayerStateReset>(_input, _plStatus, this))
                {
                    SetState<PlayerStateReset>();
                }
                else
                {
                    _playerParts.transform.position = _activeCheckPoint.transform.position + PlayerStateReset.SpawnOffset;
                }
            }
        }

        #endregion

        protected override void Init()
        {
            if (!_input)
            {
                _input = GetComponent<PlayerInput>();
            }

            if (!_playerParts)
            {
                _playerParts = GetComponent<PlayerComponents>();
            }

            if (!_plStatus)
            {
                _plStatus = GetComponent<PlayerStatus>();
            }

            if (!_playerParts)
            {
                _playerParts = GetComponent<PlayerComponents>();
            }

            if (_playerParts.Settings && _playerParts.Settings.UseSettings)
            {
                _playerParts.Rigid.mass = _playerParts.Settings.Mass;
                _playerParts.Rigid.gravityScale = _playerParts.Settings.GravityScale;
            }

            if (_activeCheckPoint == null || _activeCheckPoint.gameObject == null)
            {
                _startPositon = transform.position;
            }

            //use better method!
            CheckPointController[] tempCPs = GameObject.FindObjectsOfType<CheckPointController>();

            AllCheckPoints = new List<CheckPointController>();
            LevelCheckPoints = new List<CheckPointController>();

            foreach (var cp in tempCPs)
            {
                AllCheckPoints.Add(cp);

                if (cp.CheckPointLevelIndex == CurrentLevelIndex)
                {
                    LevelCheckPoints.Add(cp);
                }
            }
        }

        public void RecoverToHealthAndToEnergy(float percentHealth = 100, float percentEnergy = 100)
        {
            _plStatus.SetHealth((_playerParts.Settings.BaseHealth / 100) * percentHealth, true, false);
            _plStatus.CurrentEnergy = (_playerParts.Settings.BaseEnergy / 100) * percentEnergy;
        }
        public void GetHealth(float amount)
        {
            if (amount < 0)
            {
                return;
            }
            _plStatus.SetHealth(_plStatus.CurrentHealth + amount, true, false, true, false);
        }

        public void EnableStateWallSlide()
        {
            SetCanEnterState<PlayerStateWallSlide>(true);
        }
        public void EnableStateDash()
        {
            SetCanEnterState<PlayerStateDash>(true);
        }
        public void EnableStateWallJump()
        {
            SetCanEnterState<PlayerStateWallJump>(true);
        }
        public void EnableStateLedgeHang()
        {
            SetCanEnterState<PlayerStateLedgeHang>(true);
        }
        public void EnableStateMeleeAttack()
        {
            SetCanEnterState<PlayerStateAxeAttack>(true);
        }
        public void EnableStateLedgeClimb()
        {
            SetCanEnterState<PlayerStateLedgeClimb>(true);
        }

        private void SetCanEnterState<PlayerState>(bool canEnter) where PlayerState : PlayerBaseState
        {
            PlayerBaseState stateComponent = GetComponent<PlayerState>();

            if (stateComponent)
            {
                stateComponent.CanEnterState = canEnter;
            }
            else
            {
                Debug.Log("State cant be set to enter");
            }
        }



        #region TriggerMethods

        public void OnCheckPointContact(CheckPointController cp)
        {
            if (_activeCheckPoint)
            {
                _activeCheckPoint.SetInactive();
            }

            _activeCheckPoint = cp;
            _activeCheckPoint.SetActive();

            if (cp.IsFinish)
            {
                _plStatus.LevelComplete = true;
            }
        }

        public void OnMeleeWeaponHitTarget(bool movesPlayer)
        {
            if (_plStatus.FacesRight())
            {
                _playerParts.Rigid.velocity = new Vector2(_playerParts.Rigid.velocity.x - 4.9f, _playerParts.Rigid.velocity.y);
            }
            else
            {
                _playerParts.Rigid.velocity = new Vector2(_playerParts.Rigid.velocity.x + 4.9f, _playerParts.Rigid.velocity.y);
            }
        }

        public void OnBouncePlatformContact(float yForce)
        {
            _playerParts.Rigid.velocity = new Vector2(_playerParts.Rigid.velocity.x, yForce);
        }

        public void OnGoombaTopJump(Vector2 vel)
        {
            if (_playerParts.Rigid.velocity.x <= 0)
            {
                vel.x *= -1;
            }

            _playerParts.Rigid.velocity = vel;
            _playerParts.AnimController.TriggerGlobalEffectsOnEnemyTopJump();
        }

        public void OnSimpleBirdTopJump(float velY)
        {
            _playerParts.Rigid.velocity = new Vector2(_playerParts.Rigid.velocity.x, velY);
            _playerParts.AnimController.TriggerGlobalEffectsOnEnemyTopJump();
        }

        public void OnDeadlyFloorContact(Vector2 contactPos)
        {
            float dmg = 34;

            if (_plStatus.CurrentHealth - dmg > 0)
            {
                if (PlayerBaseState.CheckEntryConditions<PlayerStateDeadlyFloorReset>(_input, _plStatus, this))
                {
                    bool setSlowMo = false;
                    if (currentState is PlayerStateHurt == false || currentState is PlayerStateReset == false)
                    {
                        setSlowMo = true;
                    }
                    _plStatus.SetHealth(_plStatus.CurrentHealth - dmg, true, true, true, setSlowMo);

                    SetState<PlayerStateDeadlyFloorReset>();
                }
            }
            else
            {
                if (PlayerBaseState.CheckEntryConditions<PlayerStateHurt>(_input, _plStatus, this))
                {
                    bool setSlowMo = false;
                    if (currentState is PlayerStateHurt == false || currentState is PlayerStateReset == false)
                    {
                        setSlowMo = true;
                    }
                    _plStatus.SetHealth(0, true, true, true, setSlowMo);

                    _playerParts.AnimController.PlayBloodEffect();
                    _playerParts.AnimController.LeaveBloodstain(new Vector3(contactPos.x, contactPos.y, 0), new Vector3(0, 0, UnityEngine.Random.Range(0, 360) * 1f), 1, null, false);
                }
            }
        }

        public void OnGoombaAttackSide(float damage, Vector2 forcePush)
        {
            if (_plStatus.SetHealth(_plStatus.CurrentHealth - damage, true, true, false, true))
            {
                _playerParts.Rigid.velocity = forcePush;

                _playerParts.AnimController.PlayBloodEffect();

                if (PlayerBaseState.CheckEntryConditions<PlayerStateStunned>(_input, _plStatus, this))
                {
                    SetState<PlayerStateStunned>();
                }
            }            
        }

        public void OnSimpleBirdContact(float damage, Vector2 forcePush)
        {
            if (_plStatus.SetHealth(_plStatus.CurrentHealth - damage, true, true, false, true))
            {                
                _playerParts.Rigid.velocity = forcePush;

                _playerParts.AnimController.PlayBloodEffect();

                if (PlayerBaseState.CheckEntryConditions<PlayerStateStunned>(_input, _plStatus, this))
                {
                    SetState<PlayerStateStunned>();
                }
            }
        }

        public void OnAxeTrapContact(Vector2 contactPos, Transform axeObject)
        {
            if (_plStatus.SetHealth(_plStatus.CurrentHealth - 33, true, true, false, true))
            {
                _playerParts.AnimController.PlayBloodEffect();
                _playerParts.AnimController.LeaveBloodstain(new Vector3(contactPos.x, contactPos.y, 0), new Vector3(0, 0, UnityEngine.Random.Range(0, 360) * 1f), 0.3f, axeObject, true);

                if (PlayerBaseState.CheckEntryConditions<PlayerStateStunned>(_input, _plStatus, this))
                {
                    SetState<PlayerStateStunned>();
                }
            }
        }

        #endregion
    }
}