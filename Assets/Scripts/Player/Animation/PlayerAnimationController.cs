﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    public class PlayerAnimationController : MonoBehaviour
    {
        [Header("Settings")]
        public PlayerAnimationSettings AnimSettings;

        [Header("Sprites")]
        [SerializeField]
        private SpriteRenderer MeleeWeaponSprite;

        [Header("Particle Systems")]
        [SerializeField]
        private ParticleSystem BloodEffect;
        [SerializeField]
        private ParticleSystem GroundDustEffect;
        [SerializeField]
        private ParticleSystem WeaponEffect;
        [SerializeField]
        private List<ParticleSystem> WallSlideEffects;

        [Header("Shader Controller")]
        [SerializeField]
        private DissolveController DissolveEffect;
        [SerializeField]
        private GlowController GlowEffect;

        [Header("Bloodstains")]
        [SerializeField]
        private GameObject BloodstainGrp;
        [SerializeField]
        private GameObject BloodstainOnObjectPrefab;
        [SerializeField]
        private GameObject BloodstainOnGroundPrefab;

        private GlobalEffectsHelper _globalEffects;

        private void Start()
        {
            _globalEffects = FindObjectOfType<GlobalEffectsHelper>();
        }

        public void PlayBloodEffect()
        {
            if (BloodEffect)
            {
                BloodEffect.Play();

            }
        }
        public void StopBloodEffect()
        {
            if (BloodEffect)
            {
                BloodEffect.Stop(true, ParticleSystemStopBehavior.StopEmitting);

            }
        }

        public void LeaveBloodstain(Vector3 position, Vector3 rotation, float scale, Transform alternativeParent = null, bool leaveOnObject = false)
        {
            if (BloodstainOnObjectPrefab && BloodstainGrp)
            {
                GameObject c = null;
                if (leaveOnObject)
                {
                    c = GameObject.Instantiate(BloodstainOnObjectPrefab, null);
                }
                else
                {
                    c = GameObject.Instantiate(BloodstainOnGroundPrefab, null);
                }
                c.transform.position = position;
                c.transform.localScale = new Vector3(scale, scale, scale);
                c.transform.Rotate(rotation);
                c.transform.SetParent(alternativeParent ?? BloodstainGrp.transform, true);
            }
        }

        public void TriggerGlobalEffectsOnMeleeWeaponHit()
        {
            _globalEffects.ShakeCam();
            _globalEffects.SetSlowMo(AnimSettings.OnMeleeHitSloMoAmount, AnimSettings.OnMeleeHitSloMoLength);
        }
        public void TriggerGlobalEffectsOnEnemyTopJump()
        {
            _globalEffects.ShakeCam();
            _globalEffects.SetSlowMo(0.5f, 0.05f);
        }

        public void MeleeWeaponEnable(bool enabled = true)
        {
            MeleeWeaponSprite.enabled = enabled;
        }

        public void GlowStart(float length, Color? colorHDR = null)
        {
            GlowEffect.StartEffect(length, 0.99f, colorHDR ?? AnimSettings.DissolveColor);
        }
        public void GlowSet(float amount, Color? colorHDR = null)
        {
            GlowEffect.SetGlow(amount, colorHDR ?? AnimSettings.DissolveColor);
        }
        public void GlowResetSprites(bool visible = true, Color? colorHDR = null)
        {
            GlowEffect.ResetSprites(colorHDR ?? AnimSettings.DissolveColor, visible);
        }

        public void DissolveStart(float length, Color? colorHDR = null)
        {
            DissolveEffect.StartDissolve(length, colorHDR ?? AnimSettings.DissolveColor);
        }
        public void DissolveSet(float amount, Color? colorHDR = null)
        {
            DissolveEffect.SetDissolve(amount, colorHDR ?? AnimSettings.DissolveColor);
        }
        public void DissolveResetSprites(bool visible = true, Color? colorHDR = null)
        {
            DissolveEffect.ResetSprites(colorHDR ?? AnimSettings.DissolveColor, visible);
        }

        public void PlayGroundDustEffect()
        {
            GroundDustEffect.Play();
        }
        public void PauseGroundDustEffect()
        {
            GroundDustEffect.Stop(true, ParticleSystemStopBehavior.StopEmitting);
        }
        public void PlayWeaponEffect()
        {
            WeaponEffect.Play();
        }
        public void PauseWeaponEffect()
        {
            WeaponEffect.Stop(true, ParticleSystemStopBehavior.StopEmitting);
        }

        public void PlayWallSlideEffect()
        {
            foreach (var e in WallSlideEffects)
            {
                e.Play();
            }
        }
        public void PauseWallSlideEffect()
        {
            foreach (var e in WallSlideEffects)
            {
                e.Stop(true, ParticleSystemStopBehavior.StopEmitting);
            }
        }
    }
}