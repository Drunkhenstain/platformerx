﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    [RequireComponent(typeof(PlayerComponents))]
    public class PlayerStatus : MonoBehaviour
    {
        private PlayerComponents _playerParts;

        public bool IsRising;
        public bool IsFalling;
        public bool IsGrounded;
        public bool IsGroundedSlippery;
        public bool IsOnWallLeft;
        public bool IsOnWallRight;
        public bool IsWallSlippery;
        public bool IsOnLedgeLeft;
        public bool IsOnLedgeRight;


        public bool CanMeeleeAttack;
        public bool CanDash;

        public bool IsOnLowLedgeLeft;
        public bool IsOnLowLedgeRight;

        public int DeathCount;
        public bool LevelComplete;
        public bool DoResetWithHealth;

        [SerializeField]
        private float _ledgeDistanceCheck;
        [SerializeField]
        private float _wallDistanceCheck;
        [SerializeField]
        private float _groundedDistanceCheck;
        [SerializeField]
        private float _energyLoadingRate;
        [SerializeField]
        private float _baseHealth;
        [SerializeField]
        private float _baseEnergy;

        public float CurrentHealth;
        public float CurrentEnergy;

        private float _currentImmunityTime;
        private float _immunityAfterAttackTime;

        private float _currentCantAttackAfterAttackTime;
        private float _cantAttackAfterAttackTime;

        public bool DrawGizmoz;



        public bool SetHealth(float to, bool setDissolve = true, bool isAttack = true, bool ignoreImmunity = false, bool triggerSlowMo = false, bool triggerBloodOnAttack = true, bool triggerCanShakeOnAttack = true)
        {

            if (ignoreImmunity || _currentImmunityTime <= 0 || to > CurrentHealth)
            {
                if (isAttack)
                {
                    _currentImmunityTime = _immunityAfterAttackTime;

                    CurrentHealth = to <= _baseHealth ? to : _baseHealth;

                    if (setDissolve)
                    {
                        float amount = (CurrentHealth / _baseHealth) * 1.3f;
                        amount = amount > 0.99f ? 0.99f : amount < 0.01f ? 0.01f : amount;
                        if (_playerParts.AnimController)
                        {
                            _playerParts.AnimController.DissolveSet(amount);
                        }
                    }

                    if (triggerSlowMo)
                    {
                        if (CurrentHealth <= 0)
                        {
                            _playerParts.GlobalEffects.SetSlowMo(0.2f, 0.5f);
                        }
                        else
                        {
                            _playerParts.GlobalEffects.SetSlowMo(0.2f, 0.2f);
                        }
                    }

                    if (triggerBloodOnAttack)
                    {
                        _playerParts.AnimController.PlayBloodEffect();
                    }

                    if (triggerCanShakeOnAttack)
                    {
                        _playerParts.GlobalEffects.ShakeCam();
                    }
                    return true;
                }
                else
                {
                    CurrentHealth = to <= _baseHealth ? to : _baseHealth;
                    if (setDissolve)
                    {
                        float amount = (CurrentHealth / _baseHealth) * 1.3f;
                        amount = amount > 0.99f ? 0.99f : amount < 0.01f ? 0.01f : amount;
                        if (_playerParts.AnimController)
                        {
                            _playerParts.AnimController.DissolveSet(amount);
                        }
                    }

                    if (triggerSlowMo)
                    {
                        _playerParts.GlobalEffects.SetSlowMo(0.2f, 0.2f);
                    }
                    return true;
                }
            }
            return false;
        }

        public void SetHealthDissolve()
        {
            float amount = (CurrentHealth / _baseHealth) * 1.3f;
            amount = amount > 0.99f ? 0.99f : amount < 0.01f ? 0.01f : amount;
            _playerParts.AnimController.DissolveSet(amount);
        }

        public bool HasEnergy(float cost)
        {
            return (CurrentEnergy >= cost);
        }

        public bool PayEnergy(float cost)
        {
            if (HasEnergy(cost))
            {
                CurrentEnergy -= cost;
                return true;
            }
            else
            {
                return false;
            }
        }

        // Start is called before the first frame update
        void Start()
        {
            if (!_playerParts)
            {
                _playerParts = GetComponent<PlayerComponents>();
            }


            if (_playerParts.Settings && _playerParts.Settings.UseSettings)
            {
                _groundedDistanceCheck = _playerParts.Settings.GroundedDistanceCheck;
                _wallDistanceCheck = _playerParts.Settings.WallDistanceCheck;
                _ledgeDistanceCheck = _playerParts.Settings.LedgeDistanceCheck;

                _baseEnergy = _playerParts.Settings.BaseEnergy;
                _energyLoadingRate = _playerParts.Settings.EnergyLoadingRate;

                _baseHealth = _playerParts.Settings.BaseHealth;

                _immunityAfterAttackTime = _playerParts.Settings.ImmunityAfterHitTime;
                _currentImmunityTime = 0;

                _cantAttackAfterAttackTime = _playerParts.Settings.CantAttackAfterMeeleeAttackLength;
            }

            CurrentEnergy = _baseEnergy;
            CurrentHealth = _baseHealth;
        }

        void Update()
        {
            if (_currentImmunityTime > 0)
            {
                _currentImmunityTime -= Time.deltaTime;
            }


            ReloadEnergy();
            CheckGrounded();
            CheckGroundedSlippery();
            CheckWalls();

            CheckLedges();
            CheckFalling();
            CheckRising();

            CheckCanDash();
            CheckCanMeeleeAttack();
        }

        public bool FacesRight()
        {
            return _playerParts.PlayerRig.rotation.y == 0 ? true : false;
        }

        private void CheckCanDash()
        {
            if (IsGrounded || IsOnWallLeft || IsOnWallRight)
            {
                CanDash = true;
            }
        }

        private void CheckCanMeeleeAttack()
        {
            //if (CanMeeleeAttack == false && )
            //{
            //    CanMeeleeAttack = true;
            //}
        }

        private void ReloadEnergy()
        {
            if (CurrentEnergy < _baseEnergy)
            {
                CurrentEnergy += _energyLoadingRate * Time.deltaTime;
            }

            if (CurrentEnergy > _baseEnergy)
            {
                CurrentEnergy = _baseEnergy;
            }
        }

        private void CheckLowLedges()
        {
            IsOnLowLedgeLeft = false;
            IsOnLowLedgeRight = false;
        }

        private void CheckLedges()
        {
            IsOnLedgeLeft = false;
            IsOnLedgeRight = false;

            if (IsOnWallLeft)
            {
                if (Physics2D.Raycast(_playerParts.LedgeCheckLeftPos.position, Vector2.left, _ledgeDistanceCheck, _playerParts.GroundLayer) == false)
                {
                    // top wall collider must touch to hang on ledge
                    if (Physics2D.Raycast(_playerParts.WallCheckTopLeftPos.position, Vector2.left, _ledgeDistanceCheck, _playerParts.GroundLayer))
                    {
                        IsOnLedgeLeft = true;
                    }
                }
            }

            if (IsOnWallRight)
            {
                if (Physics2D.Raycast(_playerParts.LedgeCheckRightPos.position, Vector2.right, _ledgeDistanceCheck, _playerParts.GroundLayer) == false)
                {
                    // top wall collider must touch to hang on ledge
                    if (Physics2D.Raycast(_playerParts.WallCheckTopRightPos.position, Vector2.right, _ledgeDistanceCheck, _playerParts.GroundLayer))
                    {
                        IsOnLedgeRight = true;
                    }
                }
            }
        }

        private void CheckWalls()
        {
            // set all to false
            IsOnWallRight = false;
            IsOnWallLeft = false;
            IsWallSlippery = false;

            // LEFT
            if (Physics2D.BoxCast(_playerParts.BaseCollider.bounds.center, _playerParts.BaseCollider.bounds.size, 0f, Vector2.left, _wallDistanceCheck, _playerParts.GroundLayer.value))
            {
                IsOnWallLeft = true;
            }
            // LEFT
            if (Physics2D.BoxCast(_playerParts.BaseCollider.bounds.center, _playerParts.BaseCollider.bounds.size, 0f, Vector2.left, _wallDistanceCheck, _playerParts.SlipperyGroundLayer.value))
            {
                IsOnWallLeft = true;
                IsWallSlippery = true;
            }

            // RIGHT
            if (Physics2D.BoxCast(_playerParts.BaseCollider.bounds.center, _playerParts.BaseCollider.bounds.size, 0f, Vector2.right, _wallDistanceCheck, _playerParts.GroundLayer.value))
            {
                IsOnWallRight = true;
            }
            // RIGHT
            if (Physics2D.BoxCast(_playerParts.BaseCollider.bounds.center, _playerParts.BaseCollider.bounds.size, 0f, Vector2.right, _wallDistanceCheck, _playerParts.SlipperyGroundLayer.value))
            {
                IsOnWallRight = true;
                IsWallSlippery = true;
            }
        }

        private void CheckGrounded()
        {
            IsGrounded = false;
            if (Physics2D.BoxCast(_playerParts.BaseCollider.bounds.center, _playerParts.BaseCollider.bounds.size, 0f, Vector2.down, _groundedDistanceCheck, _playerParts.GroundLayer.value))
            {
                IsGrounded = true;
            }
        }

        private void CheckGroundedSlippery()
        {
            if (Physics2D.BoxCast(_playerParts.BaseCollider.bounds.center, _playerParts.BaseCollider.bounds.size, 0f, Vector2.down, _groundedDistanceCheck, _playerParts.SlipperyGroundLayer.value))
            {
                IsGroundedSlippery = true;
                IsGrounded = true;
            }
            else
            {
                IsGroundedSlippery = false;
            }
        }

        private void CheckFalling()
        {
            IsFalling = false;
            if (IsGrounded == false && _playerParts.Rigid.velocity.y < -0.1f)
            {
                IsFalling = true;
            }
        }

        private void CheckRising()
        {
            IsRising = false;
            if (_playerParts.Rigid.velocity.y > 0)
            {
                IsRising = true;
            }
        }

        //void OnDrawGizmos()
        //{

        //    if (DrawGizmoz)
        //    {
        //        if (_playerParts)
        //        {
        //            Gizmos.color = Color.green;

        //            Gizmos.DrawLine(_playerParts.WallCheckTopLeftPos.position, _playerParts.WallCheckTopLeftPos.position + (Vector3.left * _ledgeDistanceCheck));
        //            Gizmos.DrawLine(_playerParts.WallCheckTopRightPos.position, _playerParts.WallCheckTopRightPos.position + (Vector3.right * _ledgeDistanceCheck));

        //            Gizmos.DrawLine(_playerParts.LedgeCheckLeftPos.position, _playerParts.LedgeCheckLeftPos.position + (Vector3.left * _ledgeDistanceCheck));
        //            Gizmos.DrawLine(_playerParts.LedgeCheckRightPos.position, _playerParts.LedgeCheckRightPos.position + (Vector3.right * _ledgeDistanceCheck));
        //        }
        //    }
        //}
    }
}