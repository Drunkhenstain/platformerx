﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    public class PlayerComponents : MonoBehaviour
    {
        public void Start()
        {
            if (GlobalEffects == null)
            {
                GlobalEffects = GameObject.FindObjectOfType<GlobalEffectsHelper>();
            }
            if (GlobalBlackboard == null)
            {
                GlobalBlackboard = GameObject.FindObjectOfType<GlobalBlackboard>();
            }
        }

        [Header("Settings")]
        public PlayerSettings Settings;

        [Header("Status")]
        public PlayerStatus PlayerStatus;

        [Header("Global Level, will try to find in Scene if not set")]
        public GlobalEffectsHelper GlobalEffects;
        public GlobalBlackboard GlobalBlackboard;

        [Header("Cameras")]
        public Cinemachine.CinemachineVirtualCamera Cam;
        public Transform CamLookAt;

        [Header("Rig")]
        public Transform PlayerRig;


        [Header("Weapons")]
        public SwordTrigger SwordTrigger;

        [Header("Sprites")]
        public SpriteRenderer PlayerSprite;

        [Header("Colliders")]
        public Collider2D BaseCollider;

        [Header("Rigidbodies")]
        public Rigidbody2D Rigid;

        [Header("Trails")]
        public TrailRenderer Trail;

        [Header("Particle Systems")]
        public ParticleSystem JumpEffect;
        public ParticleSystem HurtEffect;

        [Header("Animations")]
        public Animator Anim;
        public Animator MeleeAnim;
        public PlayerAnimationController AnimController;

        [Header("Checks")]
        public Transform WallCheckTopLeftPos;
        public Transform WallCheckTopRightPos;
        public Transform WallCheckBottomLeftPos;
        public Transform WallCheckBottomRightPos;
        public Transform LedgeCheckLeftPos;
        public Transform LedgeCheckRightPos;

        [Header("Layers")]
        public LayerMask GroundLayer;
        public LayerMask SlipperyGroundLayer;
    }
}