﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    public float LookInputX;
    public float LookInputY;

    public float InputX;
    public float InputXAbs;
    public float InputXRaw;

    public float InputY;
    public float InputYAbs;
    public float InputYRaw;

    public bool JumpBtnKeydown;
    public bool JumpBtnKeyup;
    public bool JumpBtnKeyPressed;

    public bool DashBtnKeydown;
    public bool DashBtnKeyup;
    public bool DashBtnKeyPressed;

    public bool AttackBtnKeydown;
    public bool AttackBtnKeyup;
    public bool AttackBtnKeyPressed;

    [SerializeField]
    private float _jumpBtnBuffer = 0.12f;

    private float _curJumpBtnBuffer;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GetInputs();
    }

    public void ResetJumpBtn()
    {
        JumpBtnKeydown = false;
        _curJumpBtnBuffer = 0;
    }

    private void GetInputs()
    {
        LookInputX = Input.GetAxis("Horizontal2");
        LookInputY = Input.GetAxis("Vertical2");


        InputX = Input.GetAxis("Horizontal");
        InputXAbs = Mathf.Abs(InputX);
        InputXRaw = InputX == 0 ? 0 : Mathf.Sign(InputX);

        InputY = Input.GetAxis("Vertical");
        InputYAbs = Mathf.Abs(InputY);
        InputYRaw = InputY == 0 ? 0 : Mathf.Sign(InputY);

        JumpBtnKeydown = Input.GetButtonDown("Jump");
        JumpBtnKeyup = Input.GetButtonUp("Jump");
        JumpBtnKeyPressed = Input.GetButton("Jump");

        DashBtnKeydown = Input.GetButtonDown("Dash");
        DashBtnKeyup = Input.GetButtonUp("Dash");
        DashBtnKeyPressed = Input.GetButton("Dash");

        AttackBtnKeydown = Input.GetButtonDown("Attack");
        AttackBtnKeyup = Input.GetButtonUp("Attack");
        AttackBtnKeyPressed = Input.GetButton("Attack");

        if (JumpBtnKeydown)
        {
            _curJumpBtnBuffer = _jumpBtnBuffer;
        }

        if (_curJumpBtnBuffer > 0)
        {
            _curJumpBtnBuffer -= Time.deltaTime;
            JumpBtnKeydown = true;
        }
    }
}
