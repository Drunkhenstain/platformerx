﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    public class PlayerStateRun : PlayerBaseState
    {

        [SerializeField]
        private float curVel;

        private float _moveSpeed;
        private float _dampingOnStop;
        private float _dampingOnTurn;
        private float _dampingOnAccelerate;

        protected override bool OnCheckEntryConditions(PlayerInput input, PlayerStatus plStatus, PlayerStateController machine)
        {
            if (CanEnterState && plStatus.IsGrounded && input.InputXRaw != 0 && plStatus.IsGroundedSlippery == false)
            {
                return true;
            }
            return false;
        }


        public override void OnStateEnter()
        {
            base.OnStateEnter();

            _playerParts.Anim.Play(StateName);

            if (_playerParts.AnimController)
            {
                _playerParts.AnimController.PlayGroundDustEffect();
            }

            if (_playerParts.Trail)
            {
                _playerParts.Trail.emitting = false;
            }

            _playerParts.Rigid.gravityScale = _playerParts.Settings.GravityScaleOnGround;
        }

        public override void ApplySettings()
        {
            StateName = "Run";
            _moveSpeed = _playerParts.Settings.BaseMoveSpeed;

            _dampingOnStop = _playerParts.Settings.GroundSpeedModifierOnStop;
            _dampingOnTurn = _playerParts.Settings.GroundSpeedModifierOnTurn;

            _dampingOnAccelerate = _playerParts.Settings.GroundSpeedModifierOnAccelerate;
        }

        public override void CheckExitConditions()
        {
            if (CheckEntryConditions<PlayerStateAxeAttack>(_input, _plStatus, Machine as PlayerStateController))
            {
                Machine.SetState<PlayerStateAxeAttack>();
                return;
            }

            if (CheckEntryConditions<PlayerStateJump>(_input, _plStatus, Machine as PlayerStateController))
            {
                Machine.SetState<PlayerStateJump>();
                return;
            }

            if (CheckEntryConditions<PlayerStateMoveAir>(_input, _plStatus, Machine as PlayerStateController))
            {
                Machine.SetState<PlayerStateMoveAir>();
                return;
            }

            if (CheckEntryConditions<PlayerStateSlipOnFloor>(_input, _plStatus, Machine as PlayerStateController))
            {
                Machine.SetState<PlayerStateSlipOnFloor>();
                return;
            }

            if (CheckEntryConditions<PlayerStateIdle>(_input, _plStatus, Machine as PlayerStateController))
            {
                Machine.SetState<PlayerStateIdle>();
                return;
            }

            if (CheckEntryConditions<PlayerStateDash>(_input, _plStatus, Machine as PlayerStateController))
            {
                Machine.SetState<PlayerStateDash>();
                return;
            }
        }

        public override void Update()
        {
            base.Update();
            if (ExecuteStateLogic)
            {
                if (_input.InputX > 0)
                {
                    _playerParts.PlayerSprite.flipX = false;

                    Quaternion target = Quaternion.Euler(0, 0, 0);
                    _playerParts.PlayerRig.rotation = target;
                }
                else if (_input.InputX < 0)
                {
                    _playerParts.PlayerSprite.flipX = true;

                    Quaternion target = Quaternion.Euler(0, 180, 0);
                    _playerParts.PlayerRig.rotation = target;
                }
            }
        }

        public override void FixedUpdate()
        {
            if (ExecuteStateLogic)
            {
                if (_plStatus.IsGroundedSlippery == false)
                {
                    MovingHelper.MoveRigid2DCustom(_playerParts.Rigid, _input.InputX, _moveSpeed, _dampingOnAccelerate, _dampingOnTurn, _dampingOnStop, true);
                }

                _playerParts.Anim.SetFloat("Move", Mathf.Abs(_playerParts.Rigid.velocity.x));
            }
        }

        public override void OnStateExit()
        {
            if (_playerParts.AnimController)
            {
                _playerParts.AnimController.PauseGroundDustEffect();
            }

            if (_playerParts.Trail)
            {
                _playerParts.Trail.emitting = true;
            }

            _playerParts.Rigid.velocity = new Vector2(_playerParts.Rigid.velocity.x, 0);
            _playerParts.Rigid.gravityScale = _playerParts.Settings.GravityScale;
        }
    }
}