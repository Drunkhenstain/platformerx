﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    public class PlayerStateReset : PlayerBaseState
    {
        [SerializeField]
        public static Vector3 SpawnOffset = new Vector3(0, 0.2f, 0);


        protected override bool OnCheckEntryConditions(PlayerInput input, PlayerStatus plStatus, PlayerStateController machine)
        {
            return CanEnterState;
        }

        public override void ApplySettings()
        {
            StateName = "Reset";
        }

        public override void OnStateEnter()
        {
            base.OnStateEnter();

            _playerParts.BaseCollider.enabled = false;

            if (_playerParts.Trail)
            {
                _playerParts.Trail.emitting = false;
            }
            _playerParts.transform.position = MachinePlayer.GetActiveCheckpointPosition + SpawnOffset;

            _playerParts.Rigid.velocity = Vector2.zero;
        }


        public override void CheckExitConditions()
        {
            if (true)
            {
                Machine.SetState(Machine.StartingState);
            }
        }

        public override void Update()
        {
            base.Update();
            if (ExecuteStateLogic)
            {

            }
        }

        public override void FixedUpdate()
        {
            if (ExecuteStateLogic)
            {

            }
        }

        public override void OnStateExit()
        {
            if (_playerParts)
            {
                if (_playerParts.Trail)
                {
                    _playerParts.Trail.emitting = true;
                }
                MachinePlayer.RecoverToHealthAndToEnergy(100, 100);

                _playerParts.AnimController.DissolveResetSprites();
                _playerParts.BaseCollider.enabled = true;
                _playerParts.GlobalBlackboard.PlayerDied();
            }
        }
    }
}