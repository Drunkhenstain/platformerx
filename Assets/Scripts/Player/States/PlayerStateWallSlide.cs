﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    public class PlayerStateWallSlide : PlayerBaseState
    {
        private float _slipperySlidingSpeed;
        private float _jumpBreakFactor;
        private float _slidingSpeed;

        private bool _isWallrun;

        protected override bool OnCheckEntryConditions(PlayerInput input, PlayerStatus plStatus, PlayerStateController machine)
        {
            //(plStatus.IsGrounded == false && (plStatus.IsOnWallRight || plStatus.IsOnWallLeft))
            if (CanEnterState == false)
            {
                return false;
            }
            if ((plStatus.IsOnWallLeft && input.InputXRaw < 0) || (plStatus.IsOnWallRight && input.InputXRaw > 0))
            { 
                return true;
            }
            return false;
        }

        public override void OnStateEnter()
        {
            base.OnStateEnter();

            if (_plStatus.IsRising)
            {
                _playerParts.Anim.Play("WallRun");
                _isWallrun = true;
            }
            else
            {
                _playerParts.Anim.Play(StateName);
                _playerParts.AnimController.PlayWallSlideEffect();
                _isWallrun = false;
            }

            if (_plStatus.IsOnWallLeft)
            {
                _playerParts.PlayerSprite.flipX = false;
            }
            else if (_plStatus.IsOnWallRight)
            {
                _playerParts.PlayerSprite.flipX = true;
            }
        }
        public override void ApplySettings()
        {
            StateName = "WallSlide";
            _slidingSpeed = _playerParts.Settings.WallSlidingSpeed;
            _slipperySlidingSpeed = _playerParts.Settings.SlipperyWallSlidingSpeed;
            _jumpBreakFactor = _playerParts.Settings.JumpBreakFactor;
        }

        public override void CheckExitConditions()
        {
            if (CheckEntryConditions<PlayerStateRun>(_input, _plStatus, Machine as PlayerStateController))
            {
                Machine.SetState<PlayerStateRun>();
                return;
            }

            if (CheckEntryConditions<PlayerStateIdle>(_input, _plStatus, Machine as PlayerStateController))
            {
                Machine.SetState<PlayerStateIdle>();
                return;
            }

            if (CheckEntryConditions<PlayerStateLedgeHang>(_input, _plStatus, Machine as PlayerStateController))
            {
                Machine.SetState<PlayerStateLedgeHang>();
                return;
            }

            if (CheckEntryConditions<PlayerStateWallJump>(_input, _plStatus, Machine as PlayerStateController))
            {
                Machine.SetState<PlayerStateWallJump>();
                return;
            }

            if (CheckEntryConditions<PlayerStateDash>(_input, _plStatus, Machine as PlayerStateController))
            {
                Machine.SetState<PlayerStateDash>(); 
                return;
            }

            if (!_plStatus.IsOnWallRight && !_plStatus.IsOnWallLeft)
            {
                if (CheckEntryConditions<PlayerStateMoveAir>(_input, _plStatus, Machine as PlayerStateController))
                {
                    Machine.SetState<PlayerStateMoveAir>();
                    return;
                }
            }

            if (_input.InputYRaw == -1 && _input.JumpBtnKeydown == true)
            {
                if (CheckEntryConditions<PlayerStateMoveAir>(_input, _plStatus, Machine as PlayerStateController))
                {
                    Machine.SetState<PlayerStateMoveAir>();
                    return;
                }
            }
        }

        public override void Update()
        {
            base.Update();
            if (ExecuteStateLogic)
            {
                _playerParts.Anim.SetFloat("MoveVertical", Mathf.Abs(_playerParts.Rigid.velocity.y));

                // when rising play other anim and check for jumpbtnRelease
                if (_plStatus.IsRising)
                {
                    if (_plStatus.IsOnWallLeft)
                    {
                        _playerParts.PlayerSprite.flipX = true;
                        Quaternion target = Quaternion.Euler(0, 180, 0);
                        _playerParts.PlayerRig.rotation = target;
                    }
                    else if (_plStatus.IsOnWallRight)
                    {
                        _playerParts.PlayerSprite.flipX = false;
                        Quaternion target = Quaternion.Euler(0, 0, 0);
                        _playerParts.PlayerRig.rotation = target;
                    }
                }
                else
                {
                    if (_plStatus.IsOnWallLeft)
                    {
                        _playerParts.PlayerSprite.flipX = false;
                        Quaternion target = Quaternion.Euler(0, 0, 0);
                        _playerParts.PlayerRig.rotation = target;
                    }
                    else if (_plStatus.IsOnWallRight)
                    {
                        _playerParts.PlayerSprite.flipX = true;
                        Quaternion target = Quaternion.Euler(0, 180, 0);
                        _playerParts.PlayerRig.rotation = target;
                    }
                }
            }
        }

        public override void FixedUpdate()
        {
            if (ExecuteStateLogic)
            {
                // when rising play other anim and check for jumpbtnRelease
                if (_plStatus.IsRising)
                {
                    if (_isWallrun == false)
                    {
                        _playerParts.Anim.Play("WallRun");
                        _isWallrun = true;
                    }

                    if (_input.JumpBtnKeyup /*_input.JumpBtnKeypressed == false && Machine.GetLastState is PlayerStateMoveAir*/)
                    {
                        _playerParts.Rigid.velocity = new Vector2(_playerParts.Rigid.velocity.x, _playerParts.Rigid.velocity.y * _jumpBreakFactor);
                    }
                }
                else // is falling
                {
                    if (_isWallrun)
                    {
                        _isWallrun = false;
                        _playerParts.Anim.Play(StateName);
                        _playerParts.AnimController.PlayWallSlideEffect();
                    }

                    SlideWall();
                }
            }
        }

        public override void OnStateExit()
        {
            _playerParts.AnimController.PauseWallSlideEffect();
        }

        private void SlideWall()
        {
            if (_plStatus.IsFalling && _playerParts.Rigid.velocity.y < -_slidingSpeed && _plStatus.IsWallSlippery == false)
            {
                _playerParts.Rigid.velocity = new Vector2(_playerParts.Rigid.velocity.x, -_slidingSpeed);
            }
            else if (_plStatus.IsFalling && _playerParts.Rigid.velocity.y < -_slipperySlidingSpeed && _plStatus.IsWallSlippery == true)
            {
                _playerParts.Rigid.velocity = new Vector2(_playerParts.Rigid.velocity.x, -_slipperySlidingSpeed);
            }
        }
    }
}