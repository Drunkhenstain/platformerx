﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    public class PlayerStateHurt : PlayerBaseState
    {
        private float _curDuration;

        [SerializeField]
        private float _hurtDuration;

        private float _entryGravityScale;

        protected override bool OnCheckEntryConditions(PlayerInput input, PlayerStatus plStatus, PlayerStateController machine)
        {
            return CanEnterState;
        }

        public override void OnStateEnter()
        {
            base.OnStateEnter();


            if (_playerParts.Trail)
            {
                _playerParts.Trail.emitting = false;
            }

            _playerParts.Anim.speed = 1 / _hurtDuration;
            _playerParts.Anim.Play(StateName);
            _curDuration = 0f;

            _entryGravityScale = _playerParts.Rigid.gravityScale;
            _playerParts.Rigid.gravityScale = _entryGravityScale / 2;
            _playerParts.Rigid.drag = 2.5f;

            _plStatus.DeathCount += 1;

            //_playerParts.Rigid.velocity = new Vector2(Mathf.Sign(_playerParts.Rigid.velocity.x) * 6, Mathf.Sign(_playerParts.Rigid.velocity.y) * 6);

            _playerParts.BaseCollider.enabled = false;
            if (_plStatus.CurrentHealth <= 0)
            {
                _playerParts.AnimController.DissolveStart(_hurtDuration);
            }
        }

        public override void ApplySettings()
        {
            StateName = "Hurt";

        }

        public override void CheckExitConditions()
        {
            if (_hurtDuration <= _curDuration)
            {
                if (_plStatus.CurrentHealth <= 0 && CheckEntryConditions<PlayerStateReset>(_input, _plStatus, MachinePlayer))
                {
                    Machine.SetState<PlayerStateReset>();
                }
                else
                {
                    Machine.SetState(Machine.StartingState);
                }
            }
        }

        public override void Update()
        {
            base.Update();
            if (ExecuteStateLogic)
            {
                _curDuration += Time.deltaTime;
            }
        }

        public override void FixedUpdate()
        {
            if (ExecuteStateLogic)
            {

            }
        }

        public override void OnStateExit()
        {
            _playerParts.Anim.speed = 1;
            _playerParts.Rigid.gravityScale = _entryGravityScale;
            _playerParts.Rigid.drag = 0;
            _playerParts.BaseCollider.enabled = true;

            //_playerParts.AnimController.DissolveResetSprites();
        }
    }
}
