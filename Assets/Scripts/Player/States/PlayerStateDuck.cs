﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    public class _PlayerStateDuck: PlayerBaseState
    {
        protected override bool OnCheckEntryConditions(PlayerInput input, PlayerStatus plStatus, PlayerStateController machine)
        {
            if (CanEnterState)
            {
                return true;
            }

            return false;
        }

        public override void OnStateEnter()
        {
            base.OnStateEnter();

            _playerParts.Anim.Play(StateName);

        }

        public override void ApplySettings()
        {
            StateName = "Template";
        }

        public override void CheckExitConditions()
        {

        }

        public override void Update()
        {
            base.Update();
            if (ExecuteStateLogic)
            {

            }
        }

        public override void FixedUpdate()
        {
            if (ExecuteStateLogic)
            {

            }
        }

        public override void OnStateExit()
        {

        }
    }
}