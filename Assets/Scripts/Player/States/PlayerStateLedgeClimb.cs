﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    public class PlayerStateLedgeClimb : PlayerBaseState
    {
        private float _curDuration;

        public float AnimDuration = 0.5f;
        private float _entryGravityScale;
        public override void ApplySettings()
        {
            StateName = "LedgeClimb";
            //AnimDuration = 1;
        }

        protected override bool OnCheckEntryConditions(PlayerInput input, PlayerStatus plStatus, PlayerStateController machine)
        {
            if (CanEnterState == false)
            {
                return false;
            }
            if ( input.JumpBtnKeydown && input.InputYRaw != -1 && ((plStatus.IsOnWallLeft && input.InputXRaw != 1) || (plStatus.IsOnWallRight && input.InputXRaw != -1)))
            {
                return true;
            }

            return false;
        }

        public override void OnStateEnter()
        {
            base.OnStateEnter();

            _playerParts.Anim.speed = 1 / AnimDuration;
            _playerParts.Anim.Play(StateName);
            _curDuration = 0f;
            _entryGravityScale = _playerParts.Rigid.gravityScale;
            _playerParts.Rigid.gravityScale = 0;
        }

        public override void CheckExitConditions()
        {
            if (AnimDuration <= _curDuration)
            {
                Machine.SetState<PlayerStateIdle>();
                //if (CheckEntryConditions<PlayerStateIdle>(_input, _plStatus, MachinePlayer))
                //{
                //}
                //if (CheckEntryConditions<PlayerStateRun>(_input, _plStatus, MachinePlayer))
                //{
                //    Machine.SetState<PlayerStateRun>();
                //}
                //if (CheckEntryConditions<PlayerStateMoveAir>(_input, _plStatus, MachinePlayer))
                //{
                //    Machine.SetState<PlayerStateMoveAir>();
                //}
            }
        }

        public override void OnStateExit()
        {
            _playerParts.Anim.speed = 1;
            _playerParts.Rigid.gravityScale = _entryGravityScale;

            if (_plStatus.IsOnLedgeLeft || _plStatus.IsOnWallLeft)
            {
                _playerParts.transform.position += new Vector3(-_playerParts.BaseCollider.bounds.size.x, _playerParts.BaseCollider.bounds.size.y);
            }
            else if (_plStatus.IsOnLedgeRight || _plStatus.IsOnWallRight)
            {
                _playerParts.transform.position += new Vector3(_playerParts.BaseCollider.bounds.size.x, _playerParts.BaseCollider.bounds.size.y);
            }
        }

        // Update is called once per frame
        public override void Update()
        {
            base.Update();
            if (ExecuteStateLogic)
            {
                _curDuration += Time.deltaTime;
            }
        }

        public override void FixedUpdate()
        {
            if (ExecuteStateLogic)
            {

            }
        }
    }
}
