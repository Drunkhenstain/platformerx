﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

namespace PlatformerX
{
    public class PlayerStateAxeAttack : PlayerBaseState
    {
        private float _attackLength;
        public float _slowDownFactorX;
        private float _curDuration;
        private float _maxFallingSpeed;//15

        private float _moveSpeed;
        private float _stopOnGroundModifier;
        private float _stopInAirModifier;

        private bool _isUpperStrike = false;
        private bool _isDashAttack = false;

        private SwordTrigger _weapon;

        protected override bool OnCheckEntryConditions(PlayerInput input, PlayerStatus plStatus, PlayerStateController machine)
        {
            if (CanEnterState == false)
            {
                return false;
            }
            if (input.AttackBtnKeydown)
            {
                return true;
            }
            return false;
        }


        public override void OnStateEnter()
        {
            base.OnStateEnter();

            if (_playerParts.Trail)
            {
                _playerParts.Trail.emitting = false;
            }

            _plStatus.CanMeeleeAttack = false;

            _playerParts.AnimController.MeleeWeaponEnable();
            //_playerParts.Anim.speed = 4f;
            _playerParts.Anim.speed = 1 / _attackLength;

            if (_input.InputY > 0.6f)
            {
                _isUpperStrike = true;
                _playerParts.Anim.Play("AttackMeleeUp");
            }
            else
            {
                _playerParts.Anim.Play(StateName);
            }

            _curDuration = 0;

            if (Machine.GetLastState is PlayerStateDash)
            {
                _playerParts.AnimController.GlowSet(0.99f, new Color(0.9f, 0, 4.5f, 1));
                _isDashAttack = true;
            }
        }

        public override void ApplySettings()
        {
            StateName = "AxeAttack";
            _moveSpeed = _playerParts.Settings.BaseMoveSpeed;
            _stopOnGroundModifier = _playerParts.Settings.GroundSpeedModifierOnStop;
            _weapon = _playerParts.SwordTrigger;
            _maxFallingSpeed = _playerParts.Settings.MaxFallingSpeed;
            _attackLength = _playerParts.Settings.MeeleeAttackLength;
            _stopInAirModifier = _playerParts.Settings.AirSpeedModifierOnStop;
        }

        public override void CheckExitConditions()
        {
            if (_curDuration > _attackLength)
            {
                if (CheckEntryConditions<PlayerStateIdle>(_input, _plStatus, Machine as PlayerStateController))
                {
                    Machine.SetState<PlayerStateIdle>();
                    return;
                }

                if (CheckEntryConditions<PlayerStateSlipOnFloor>(_input, _plStatus, Machine as PlayerStateController))
                {
                    Machine.SetState<PlayerStateSlipOnFloor>();
                    return;
                }

                if (CheckEntryConditions<PlayerStateRun>(_input, _plStatus, Machine as PlayerStateController))
                {
                    Machine.SetState<PlayerStateRun>();
                    return;
                }

                if (CheckEntryConditions<PlayerStateMoveAir>(_input, _plStatus, Machine as PlayerStateController))
                {
                    Machine.SetState<PlayerStateMoveAir>();
                    return;
                }
                if (CheckEntryConditions<PlayerStateAxeAttack>(_input, _plStatus, Machine as PlayerStateController))
                {
                    Machine.SetState<PlayerStateAxeAttack>();
                    return;
                }
            }
        }

        public override void Update()
        {
            base.Update();
            if (ExecuteStateLogic)
            {
                if (_weapon.EnemiesToProcess.Count > 0)
                {
                    float dmg = _isDashAttack ? 2 : 1;

                    // only move player in normal strike
                    MachinePlayer.OnMeleeWeaponHitTarget(!_isUpperStrike);

                    bool fromRight;
                    foreach (var e in _weapon.EnemiesToProcess)
                    {
                        if (e.transform.position.x > MachinePlayer.transform.position.x)
                        {
                            fromRight = false;
                        }
                        else
                        {
                            fromRight = true;
                        }
                        IAttackable atk = (e as IAttackable);
                        atk.OnWeaponAttack(dmg, fromRight);

                        if (e.CurrentHealth <= 0)
                        {
                            MachinePlayer.GetHealth(UnityEngine.Random.Range(3, e.PlayerHealthOnDeath));
                        }
                         
                    }
                    _playerParts.AnimController.TriggerGlobalEffectsOnMeleeWeaponHit();
                    _weapon.Reset();
                }
                _curDuration += Time.deltaTime;
            }
        }

        public override void FixedUpdate()
        {
            if (ExecuteStateLogic)
            {
                if (_plStatus.IsGrounded)
                {
                    MovingHelper.MoveRigid2DCustom(_playerParts.Rigid, 0, _moveSpeed, 0, 0, _stopOnGroundModifier);
                }
                else
                {
                    MovingHelper.MoveRigid2DCustom(_playerParts.Rigid, 0, _moveSpeed, 0, 0, _stopInAirModifier);


                    if (_plStatus.IsFalling && _playerParts.Rigid.velocity.y < -_maxFallingSpeed)
                    {
                        _playerParts.Rigid.velocity = new Vector2(_playerParts.Rigid.velocity.x, -_maxFallingSpeed);
                    }
                }
            }
        }

        public override void OnStateExit()
        {
            _playerParts.Anim.speed = 1;
            _playerParts.AnimController.MeleeWeaponEnable(false);
            _playerParts.AnimController.PauseWeaponEffect();

            if (Machine.GetLastState is PlayerStateDash)
            {
                _playerParts.AnimController.GlowResetSprites();
            }
        }
    }
}
