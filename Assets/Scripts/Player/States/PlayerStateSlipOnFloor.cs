﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    public class PlayerStateSlipOnFloor : PlayerBaseState
    {
        [SerializeField]
        private float _maxSlipSpeed;//375

        protected override bool OnCheckEntryConditions(PlayerInput input, PlayerStatus plStatus, PlayerStateController machine)
        {
            if (CanEnterState && plStatus.IsGroundedSlippery)
            {
                return true;
            }
            return false;
        }


        public override void OnStateEnter()
        {
            base.OnStateEnter();


            _playerParts.Anim.Play(StateName);

            if (_playerParts.Trail)
            {
                _playerParts.Trail.emitting = false;
            }
        }

        public override void ApplySettings()
        {
            StateName = "SlipOnFloor";
            _maxSlipSpeed = _playerParts.Settings.MaxSlipOnFloorSpeed;
        }

        public override void CheckExitConditions()
        {
            if (CheckEntryConditions<PlayerStateJump>(_input, _plStatus, Machine as PlayerStateController))
            {
                Machine.SetState<PlayerStateJump>();
                return;
            }

            if ((Machine.GetLastState is PlayerStateDash) == false)
            {
                if (CheckEntryConditions<PlayerStateDash>(_input, _plStatus, Machine as PlayerStateController))
                {
                    Machine.SetState<PlayerStateDash>();
                    return;
                }
            }

            if (_plStatus.IsGroundedSlippery == false)
            {
                if (CheckEntryConditions<PlayerStateMoveAir>(_input, _plStatus, Machine as PlayerStateController))
                {
                    Machine.SetState<PlayerStateMoveAir>();
                    return;
                }

                if (CheckEntryConditions<PlayerStateIdle>(_input, _plStatus, Machine as PlayerStateController))
                {
                    Machine.SetState<PlayerStateIdle>();
                    return;
                }

                if (CheckEntryConditions<PlayerStateRun>(_input, _plStatus, Machine as PlayerStateController))
                {
                    Machine.SetState<PlayerStateRun>();
                    return;
                }
            }
        }

        public override void Update()
        {
            base.Update();
            if (ExecuteStateLogic)
            {
                _playerParts.Anim.SetFloat("Move", _input.InputXAbs);

                if (_playerParts.Rigid.velocity.x > 0)
                {
                    _playerParts.PlayerSprite.flipX = false;

                    Quaternion target = Quaternion.Euler(0, 0, 0);
                    _playerParts.PlayerRig.rotation = target;
                }
                else if (_playerParts.Rigid.velocity.x < 0)
                {
                    _playerParts.PlayerSprite.flipX = true;

                    Quaternion target = Quaternion.Euler(0, 180, 0);
                    _playerParts.PlayerRig.rotation = target;
                }

            }
        }

        public override void FixedUpdate()
        {
            if (ExecuteStateLogic)
            {
                if (_plStatus.IsGroundedSlippery)
                {
                    // check max speed
                    _playerParts.Rigid.velocity = Vector2.ClampMagnitude(_playerParts.Rigid.velocity, _maxSlipSpeed);
                    //MovingHelper.MoveRigid2DDamped(_playerParts.Rigid, ref curVel, _input.InputXRaw, _accelerationTest, _dampingOnAccelerate, _dampingOnTurn, _dampingOnStop);
                }

            }
        }

        public override void OnStateExit()
        {
            if (_playerParts.Trail)
            {
                _playerParts.Trail.emitting = true;
            }
        }
    }
}