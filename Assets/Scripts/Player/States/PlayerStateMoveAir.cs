﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    public class PlayerStateMoveAir : PlayerBaseState
    {
        public List<State> StatesWithCoyoteTime;

        private float _maxFallingSpeed;//15
        private float _airMoveSpeed;   //375
        private float _airMoveTime;    //0,5
        private float _jumpBreakFactor;//0,5
        private float _jumpCoyoteTime; //0,15


        //private float _curAirMoveTime;
        private float _curJumpCoyoteTime;

        private float curVel;

        private float _accelerationTest;
        private float _dampingOnStop;
        private float _dampingOnTurn;
        private float _dampingOnAccelerate;


        protected override bool OnCheckEntryConditions(PlayerInput input, PlayerStatus plStatus, PlayerStateController machine)
        {
            if (plStatus.IsGrounded)
            {
                return false;
            }

            return CanEnterState;
        }

        public override void OnStateEnter()
        {
            base.OnStateEnter();

            StateName = "MoveAir";

            if ((Machine.GetLastState is PlayerStateJump || Machine.GetLastState is PlayerStateWallJump) == false)
            {
                _playerParts.Anim.Play("Fall");
            }

            //_curAirMoveTime = _airMoveTime;

            if (StatesWithCoyoteTime.Contains(Machine.GetLastState))
            {
                _curJumpCoyoteTime = _jumpCoyoteTime;
            }
            else
            {
                _curJumpCoyoteTime = 0;
            }

            if (_plStatus.IsRising && !_input.JumpBtnKeyPressed && Machine.GetLastState is PlayerStateJump)
            {
                _playerParts.Rigid.velocity = new Vector2(_playerParts.Rigid.velocity.x, _playerParts.Rigid.velocity.y * _jumpBreakFactor);
            }

            if (_plStatus.IsRising && !_input.JumpBtnKeyPressed && Machine.GetLastState is PlayerStateWallJump)
            {
                _playerParts.Rigid.velocity = new Vector2(_playerParts.Rigid.velocity.x, _playerParts.Rigid.velocity.y * _jumpBreakFactor);
            }
        }

        public override void ApplySettings()
        {
            _maxFallingSpeed = _playerParts.Settings.MaxFallingSpeed;

            _jumpBreakFactor = _playerParts.Settings.JumpBreakFactor;
            _jumpCoyoteTime = _playerParts.Settings.JumpCoyoteTime;

            _accelerationTest = _playerParts.Settings.BaseMoveSpeed;
            _dampingOnAccelerate = _playerParts.Settings.AirSpeedModifierOnAccelerate;
            _dampingOnStop = _playerParts.Settings.AirSpeedModifierOnStop;
            _dampingOnTurn = _playerParts.Settings.AirSpeedModifierOnTurn;
        }

        public override void CheckExitConditions()
        {
            if (CheckEntryConditions<PlayerStateSlipOnFloor>(_input, _plStatus, Machine as PlayerStateController))
            {
                Machine.SetState<PlayerStateSlipOnFloor>();
                return;
            }

            if (CheckEntryConditions<PlayerStateRun>(_input, _plStatus, Machine as PlayerStateController))
            {
                Machine.SetState<PlayerStateRun>();
                return;
            }

            if (CheckEntryConditions<PlayerStateLedgeHang>(_input, _plStatus, Machine as PlayerStateController))
            {
                Machine.SetState<PlayerStateLedgeHang>();
                return;
            }

            if (CheckEntryConditions<PlayerStateIdle>(_input, _plStatus, Machine as PlayerStateController))
            {
                Machine.SetState<PlayerStateIdle>();
                return;
            }

            if (CheckEntryConditions<PlayerStateIdle>(_input, _plStatus, Machine as PlayerStateController))
            {
                Machine.SetState<PlayerStateIdle>();
                return;
            }

            if (CheckEntryConditions<PlayerStateWallSlide>(_input, _plStatus, Machine as PlayerStateController))
            {
                Machine.SetState<PlayerStateWallSlide>();
                return;
            }

            if (CheckEntryConditions<PlayerStateAxeAttack>(_input, _plStatus, Machine as PlayerStateController))
            {
                Machine.SetState<PlayerStateAxeAttack>();
                return;
            }

            if (_curJumpCoyoteTime > 0)
            {
                if (CheckEntryConditions<PlayerStateJump>(_input, _plStatus, Machine as PlayerStateController))
                {
                    Machine.SetState<PlayerStateJump>();
                    return;
                }
            }

            if (CheckEntryConditions<PlayerStateDash>(_input, _plStatus, Machine as PlayerStateController))
            {
                Machine.SetState<PlayerStateDash>();
                return;
            }
        }

        public override void Update()
        {
            base.Update();
            if (ExecuteStateLogic)
            {
                _playerParts.Anim.SetFloat("Move", _input.InputXAbs);

                if (_playerParts.Rigid.velocity.y < -10 && _playerParts.Anim.GetCurrentAnimatorStateInfo(0).IsName("Fall") == false)
                {
                    _playerParts.Anim.Play("Fall");
                }

                if (_curJumpCoyoteTime > 0)
                {
                    _curJumpCoyoteTime -= Time.deltaTime;
                }

                if (_input.InputX > 0)
                {
                    _playerParts.PlayerSprite.flipX = false;
                    Quaternion target = Quaternion.Euler(0, 0, 0);
                    _playerParts.PlayerRig.rotation = target;
                }
                else if (_input.InputX < 0)
                {
                    _playerParts.PlayerSprite.flipX = true;
                    Quaternion target = Quaternion.Euler(0, 180, 0);
                    _playerParts.PlayerRig.rotation = target;
                }


                if (_plStatus.IsRising && _input.JumpBtnKeyup && Machine.GetLastState is PlayerStateJump)
                {
                    _playerParts.Rigid.velocity = new Vector2(_playerParts.Rigid.velocity.x, _playerParts.Rigid.velocity.y * _jumpBreakFactor);
                }

                if (_plStatus.IsRising && _input.JumpBtnKeyup && Machine.GetLastState is PlayerStateWallJump)
                {
                    _playerParts.Rigid.velocity = new Vector2(_playerParts.Rigid.velocity.x, _playerParts.Rigid.velocity.y * _jumpBreakFactor);
                }
            }
        }

        public override void FixedUpdate()
        {
            if (ExecuteStateLogic)
            {
                MovingHelper.MoveRigid2DCustom(_playerParts.Rigid, _input.InputXRaw, _accelerationTest, _dampingOnAccelerate, _dampingOnTurn, _dampingOnStop);


                //if (_plStatus.IsRising && _input.JumpBtnKeyup && Machine.GetLastState is PlayerStateJump)
                //{
                //    _playerParts.Rigid.velocity = new Vector2(_playerParts.Rigid.velocity.x, _playerParts.Rigid.velocity.y * _jumpBreakFactor);
                //}

                //if (_plStatus.IsRising && _input.JumpBtnKeyup && Machine.GetLastState is PlayerStateWallJump)
                //{
                //    _playerParts.Rigid.velocity = new Vector2(_playerParts.Rigid.velocity.x, _playerParts.Rigid.velocity.y * _jumpBreakFactor);
                //}

                if (_plStatus.IsFalling && _playerParts.Rigid.velocity.y < -_maxFallingSpeed)
                {
                    _playerParts.Rigid.velocity = new Vector2(_playerParts.Rigid.velocity.x, -_maxFallingSpeed);
                }

            }
        }

        public override void OnStateExit()
        {
            if (_playerParts.JumpEffect && _playerParts.JumpEffect.isPlaying == true)
            {
                _playerParts.JumpEffect.Stop();
            }
        }
    }
}