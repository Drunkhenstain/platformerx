﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    public class PlayerStateWallJump : PlayerBaseState
    {
        private float _jumpForce;//20
        private float _wallJumpXForceModifier;//used after wall sliding

        private float _waitForSuccessTime;
        private float _curWaitForSuccessTime;

        protected override bool OnCheckEntryConditions(PlayerInput input, PlayerStatus plStatus, PlayerStateController machine)
        {
            if (CanEnterState == false)
            {
                return false;
            }

            if ( input.JumpBtnKeydown && ((plStatus.IsOnWallLeft && input.InputXRaw > 0) || (plStatus.IsOnWallRight && input.InputXRaw < 0)))
            {
                return true;
            }

            return false;
        }

        public override void OnStateEnter()
        {
            base.OnStateEnter();


            // todo: anim state speed is set to 2, revert in jump and in walljump!!!
            _playerParts.Anim.Play(StateName);

            if (_playerParts.JumpEffect)
            {
                _playerParts.JumpEffect.Play();
            }

            // because it needs a bit of time to leave the grounded status, after this time if we are still on ground it counts as no success!!
            _curWaitForSuccessTime = _waitForSuccessTime;

            // apply force on enter depending on last state
            if ((Machine.GetLastState is PlayerStateWallSlide))
            {
                if (_plStatus.IsOnWallLeft)
                {
                    _playerParts.Rigid.velocity = new Vector2(_jumpForce * _wallJumpXForceModifier, _jumpForce);
                }
                else if (_plStatus.IsOnWallRight)
                {
                    _playerParts.Rigid.velocity = new Vector2(-_jumpForce * _wallJumpXForceModifier, _jumpForce);
                }
            }
            else if ((Machine.GetLastState is PlayerStateLedgeHang))
            {
                if ((_plStatus.IsOnLedgeLeft && _input.InputX > 0.5f) || (_plStatus.IsOnLedgeRight && _input.InputX < -0.5f))
                {
                    if (_plStatus.IsOnLedgeLeft)
                    {
                        _playerParts.Rigid.velocity = new Vector2(_jumpForce * _wallJumpXForceModifier, _jumpForce);
                    }
                    else if (_plStatus.IsOnLedgeRight)
                    {
                        _playerParts.Rigid.velocity = new Vector2(-_jumpForce * _wallJumpXForceModifier, _jumpForce);
                    }
                }
            }
        }

        public override void ApplySettings()
        {
            StateName = "WallJump";

            _jumpForce = _playerParts.Settings.JumpForce;

            _wallJumpXForceModifier = _playerParts.Settings.WallJumpXForceModifier;

            _waitForSuccessTime = _playerParts.Settings.JumpWaitForSuccessTime;
        }

        public override void CheckExitConditions()
        {
            // only check for leave if we are rising
            if (_plStatus.IsRising == true)
            {
                if (CheckEntryConditions<PlayerStateMoveAir>(_input, _plStatus, Machine as PlayerStateController))
                {
                    Machine.SetState<PlayerStateMoveAir>();
                    return;
                }
            }
            else if (_curWaitForSuccessTime <= 0)
            {
                if (CheckEntryConditions<PlayerStateIdle>(_input, _plStatus, Machine as PlayerStateController))
                {
                    Machine.SetState<PlayerStateIdle>();
                    return;
                }
                if (CheckEntryConditions<PlayerStateRun>(_input, _plStatus, Machine as PlayerStateController))
                {
                    Machine.SetState<PlayerStateRun>();
                    return;
                }
            }

            _curWaitForSuccessTime -= Time.deltaTime;
        }

        public override void Update()
        {
            if (ExecuteStateLogic)
            {
                base.Update();
            }
        }

        public override void FixedUpdate()
        {
            if (ExecuteStateLogic)
            {

            }
        }

        public override void OnStateExit()
        {
            _input.ResetJumpBtn();
        }
    }
}