﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    public class PlayerStateLedgeHang : PlayerBaseState
    {
        private Vector2 _startPosition;

        protected override bool OnCheckEntryConditions(PlayerInput input, PlayerStatus plStatus, PlayerStateController machine)
        {
            if (CanEnterState == false)
            {
                return false;
            }
            if ( plStatus.IsFalling && (plStatus.IsOnLedgeLeft || plStatus.IsOnLedgeRight))
            {
                if (input.JumpBtnKeyPressed || (plStatus.IsOnLedgeLeft && input.InputXRaw == -1) || (plStatus.IsOnLedgeRight && input.InputXRaw == 1))
                {
                    return true;
                }
            }


            return false;
        }

        public override void OnStateEnter()
        {
            base.OnStateEnter();


            if (_plStatus.IsOnLedgeLeft || _plStatus.IsOnWallLeft)
            {
                _playerParts.PlayerSprite.flipX = false;
                Quaternion target = Quaternion.Euler(0, 0, 0);
                _playerParts.PlayerRig.rotation = target;

                _playerParts.Rigid.velocity = new Vector2(-0.1f,0);
            }
            else if (_plStatus.IsOnLedgeRight || _plStatus.IsOnWallRight)
            {
                _playerParts.PlayerSprite.flipX = true;
                Quaternion target = Quaternion.Euler(0, 180, 0);
                _playerParts.PlayerRig.rotation = target;

                _playerParts.Rigid.velocity = new Vector2(0.1f, 0);
            }

            _startPosition = _playerParts.Rigid.position;

            //_playerParts.Rigid.velocity = Vector2.zero;
            _playerParts.Rigid.gravityScale = 0;

            _playerParts.Anim.Play(StateName);

            if (_playerParts.Trail)
            {
                _playerParts.Trail.emitting = false;
            }
        }

        public override void ApplySettings()
        {
            StateName = "LedgeHang";
        }

        public override void CheckExitConditions()
        {
            if (CheckEntryConditions<PlayerStateRun>(_input, _plStatus, Machine as PlayerStateController))
            {
                Machine.SetState<PlayerStateRun>();
                return;
            }

            if (CheckEntryConditions<PlayerStateIdle>(_input, _plStatus, Machine as PlayerStateController))
            {
                Machine.SetState<PlayerStateIdle>();
                return;
            }

            if (!_plStatus.IsOnWallLeft && !_plStatus.IsOnWallRight)
            {
                if (CheckEntryConditions<PlayerStateMoveAir>(_input, _plStatus, Machine as PlayerStateController))
                {
                    Machine.SetState<PlayerStateMoveAir>();
                    return;
                }
            }

            if (_input.JumpBtnKeydown)
            {
                if (CheckEntryConditions<PlayerStateWallJump>(_input, _plStatus, Machine as PlayerStateController))
                {
                    Machine.SetState<PlayerStateWallJump>();
                    return;
                }
                else if (CheckEntryConditions<PlayerStateLedgeClimb>(_input, _plStatus, Machine as PlayerStateController))
                {
                    Machine.SetState<PlayerStateLedgeClimb>();
                    return;
                }
                else if (_input.InputY == -1)
                {
                    if (CheckEntryConditions<PlayerStateMoveAir>(_input, _plStatus, Machine as PlayerStateController))
                    {
                        Machine.SetState<PlayerStateMoveAir>();
                        return;
                    }
                }
            }
        }

        public override void Update()
        {
            base.Update();

            if (ExecuteStateLogic)
            {
                if (_plStatus.IsOnLedgeLeft || _plStatus.IsOnWallLeft)
                {
                    _playerParts.PlayerSprite.flipX = false;
                    Quaternion target = Quaternion.Euler(0, 0, 0);
                    _playerParts.PlayerRig.rotation = target;
                }
                else if (_plStatus.IsOnLedgeRight || _plStatus.IsOnWallRight)
                {
                    _playerParts.PlayerSprite.flipX = true;
                    Quaternion target = Quaternion.Euler(0, 180, 0);
                    _playerParts.PlayerRig.rotation = target;
                }
            }
        }

        public override void FixedUpdate()
        {
            if (ExecuteStateLogic)
            {

            }
        }

        public override void OnStateExit()
        {
            if (_playerParts.Trail)
            {
                _playerParts.Trail.emitting = true;
            }
            _playerParts.Rigid.gravityScale = _playerParts.Settings.GravityScale;

        }
    }
}
