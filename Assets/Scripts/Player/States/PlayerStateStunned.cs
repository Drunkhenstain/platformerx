﻿
using UnityEngine;

namespace PlatformerX
{
    public class PlayerStateStunned : PlayerBaseState
    {

        private float _stunDuration;
        private float _curDuration;

        private float _maxFallingSpeed;
        private float _moveSpeed;
        private float _stopOnGroundModifier;
        private float _stopInAirModifier;

        protected override bool OnCheckEntryConditions(PlayerInput input, PlayerStatus plStatus, PlayerStateController machine)
        {
            if (CanEnterState)
            {
                return true;
            }

            return false;
        }

        public override void OnStateEnter()
        {
            base.OnStateEnter();

            _playerParts.Anim.Play(StateName);


            _playerParts.Anim.speed = 1 / _stunDuration;
            _playerParts.Anim.Play(StateName);
            _curDuration = 0f;
        }

        public override void ApplySettings()
        {
            StateName = "Stunned";
            _stunDuration = _playerParts.Settings.StunStateLength;

            _moveSpeed = _playerParts.Settings.BaseMoveSpeed;
            _stopOnGroundModifier = _playerParts.Settings.GroundSpeedModifierOnStop;
            _maxFallingSpeed = _playerParts.Settings.MaxFallingSpeed;
            _stopInAirModifier = _playerParts.Settings.AirSpeedModifierOnStop;
        }

        public override void CheckExitConditions()
        {
            if (_stunDuration <= _curDuration)
            {
                if (_plStatus.CurrentHealth <= 0 && CheckEntryConditions<PlayerStateReset>(_input, _plStatus, MachinePlayer))
                {
                    Machine.SetState<PlayerStateReset>();
                }
                else
                {
                    Machine.SetState(Machine.StartingState);
                }
            }
        }

        public override void Update()
        {
            base.Update();
            if (ExecuteStateLogic)
            {
                _curDuration += Time.deltaTime;
            }
        }

        public override void FixedUpdate()
        {
            if (ExecuteStateLogic)
            {
                if (_plStatus.IsGrounded)
                {
                    MovingHelper.MoveRigid2DCustom(_playerParts.Rigid, 0, _moveSpeed, 0, 0, _stopOnGroundModifier);
                }
                else
                {
                    MovingHelper.MoveRigid2DCustom(_playerParts.Rigid, 0, _moveSpeed, 0, 0, _stopInAirModifier);
                    if (_plStatus.IsFalling && _playerParts.Rigid.velocity.y < -_maxFallingSpeed)
                    {
                        _playerParts.Rigid.velocity = new Vector2(_playerParts.Rigid.velocity.x, -_maxFallingSpeed);
                    }
                }
            }
        }

        public override void OnStateExit()
        {
            _playerParts.Anim.speed = 1;
        }
    }
}