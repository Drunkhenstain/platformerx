﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace PlatformerX
{
    [RequireComponent(typeof(PlayerInput))]
    [RequireComponent(typeof(PlayerComponents))]
    [RequireComponent(typeof(PlayerStatus))]
    public abstract class PlayerBaseState : State
    {
        public bool CanEnterState = true;

        protected float _animSpeedModifier;
        protected PlayerInput _input;
        protected PlayerComponents _playerParts;
        protected PlayerStatus _plStatus;

        protected PlayerStateController MachinePlayer { get { return Machine as PlayerStateController; } }

        public static bool CheckEntryConditions<StateType>(PlayerInput input, PlayerStatus plStatus, PlayerStateController machine) where StateType : State
        {
            PlayerBaseState targetStateToCheck = (machine.GetState<StateType>() as PlayerBaseState);

            if (targetStateToCheck == null)
            {
                //Debug.Log("targetStateToCheck not found");
                return false;
            }

            return targetStateToCheck.OnCheckEntryConditions(input, plStatus, machine);
        }

        protected abstract bool OnCheckEntryConditions(PlayerInput input, PlayerStatus plStatus, PlayerStateController machine);

        //  Always call!
        public override void OnStateEnter()
        {
            if (!_input)
            {
                _input = GetComponent<PlayerInput>();
            }

            if (!_playerParts)
            {
                _playerParts = GetComponent<PlayerComponents>();
            }

            if (!_plStatus)
            {
                _plStatus = GetComponent<PlayerStatus>();
            }
            // move to OnstateInitialize maybe
            if (_playerParts.Settings && _playerParts.Settings.UseSettings)
            {
                ApplySettings();
            }
        }

        public override void OnStateInitialize()
        {
        }
    }
}
