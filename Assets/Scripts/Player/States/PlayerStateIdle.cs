﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    public class PlayerStateIdle : PlayerBaseState
    {
        private float _stopOnGroundModifier;

        private float _moveSpeed;

        protected override bool OnCheckEntryConditions(PlayerInput input, PlayerStatus plStatus, PlayerStateController machine)
        {
            if (CanEnterState && plStatus.IsGrounded && input.InputXRaw == 0 && _plStatus.IsGroundedSlippery == false)
            {
                return true;
            }
            return false;
        }

        public override void OnStateEnter()
        {
            base.OnStateEnter();

            if (Mathf.Abs(_playerParts.Rigid.velocity.x) > 0.1f)
            {
                _playerParts.Anim.Play("SlipOnFloor");
            }
            else
            {
                _playerParts.Anim.Play(StateName);
            }

            if (_playerParts.Trail)
            {
                _playerParts.Trail.emitting = false;
            }

            _playerParts.Rigid.gravityScale = _playerParts.Settings.GravityScaleOnGround;
        }

        public override void ApplySettings()
        {
            _stopOnGroundModifier = _playerParts.Settings.GroundSpeedModifierOnStop;
            _moveSpeed = _playerParts.Settings.BaseMoveSpeed;
            StateName = "Idle";
        }


        public override void CheckExitConditions()
        {
            if (CheckEntryConditions<PlayerStateAxeAttack>(_input, _plStatus, Machine as PlayerStateController))
            {
                Machine.SetState<PlayerStateAxeAttack>();
                return;
            }

            if (CheckEntryConditions<PlayerStateSlipOnFloor>(_input, _plStatus, Machine as PlayerStateController))
            {
                Machine.SetState<PlayerStateSlipOnFloor>();
                return;
            }

            if (CheckEntryConditions<PlayerStateRun>(_input, _plStatus, Machine as PlayerStateController))
            {
                Machine.SetState<PlayerStateRun>();
                return;
            }

            if (_plStatus.IsGrounded == false)
            {
                Machine.SetState<PlayerStateMoveAir>();
                return;
            }

            if (_input.JumpBtnKeydown)
            {
                Machine.SetState<PlayerStateJump>();
                return;
            }

            if (CheckEntryConditions<PlayerStateDash>(_input, _plStatus, Machine as PlayerStateController))
            {
                Machine.SetState<PlayerStateDash>();
                return;
            }
        }

        public override void Update()
        {
            base.Update();
            if (ExecuteStateLogic)
            {
                MovingHelper.MoveRigid2DCustom(_playerParts.Rigid, _input.InputX, _moveSpeed, 0, 0, _stopOnGroundModifier);

                if (Mathf.Abs(_playerParts.Rigid.velocity.x) <= 0.05f)
                {
                    _playerParts.Anim.Play(StateName);
                }
            }
        }

        public override void FixedUpdate()
        {
            if (ExecuteStateLogic)
            {

            }

        }

        public override void OnStateExit()
        {
            if (_playerParts.Trail)
            {
                _playerParts.Trail.emitting = true;
            }

            _playerParts.Rigid.gravityScale = _playerParts.Settings.GravityScale;
        }
    }
}