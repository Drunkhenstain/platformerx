﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    public class PlayerStateFall : PlayerBaseState
    {
        private float _maxFallingSpeed;//15


        protected override bool OnCheckEntryConditions(PlayerInput input, PlayerStatus plStatus, PlayerStateController machine)
        {
            if (CanEnterState && plStatus.IsFalling)
            {
                return true;
            }
            return false;
        }

        public override void OnStateEnter()
        {
            base.OnStateEnter();

            StateName = "Fall";

            _playerParts.Anim.Play("Fall");
        }

        public override void ApplySettings()
        {
            _maxFallingSpeed = _playerParts.Settings.MaxFallingSpeed;
        }

        public override void CheckExitConditions()
        {
            if (CheckEntryConditions<PlayerStateIdle>(_input, _plStatus, Machine as PlayerStateController))
            {
                Machine.SetState<PlayerStateIdle>();
                return;
            }

            if (CheckEntryConditions<PlayerStateRun>(_input, _plStatus, Machine as PlayerStateController))
            {
                Machine.SetState<PlayerStateRun>();
                return;
            }

            if ((_plStatus.IsOnWallLeft && _input.InputXRaw < 0) || (_plStatus.IsOnWallRight && _input.InputXRaw > 0))
            {
                if (CheckEntryConditions<PlayerStateWallSlide>(_input, _plStatus, Machine as PlayerStateController))
                {
                    Machine.SetState<PlayerStateWallSlide>();
                    return;
                }
            }
        }

        // Update is called once per frame
        public override void Update()
        {
            base.Update();
            if (ExecuteStateLogic)
            {

                if (_input.InputX > 0)
                {
                    _playerParts.PlayerSprite.flipX = false;
                }
                else if (_input.InputX < 0)
                {
                    _playerParts.PlayerSprite.flipX = true;
                }

            }
        }

        public override void FixedUpdate()
        {
            if (ExecuteStateLogic)
            {
                if (_plStatus.IsFalling && _playerParts.Rigid.velocity.y < -_maxFallingSpeed)
                {
                    _playerParts.Rigid.velocity = new Vector2(_playerParts.Rigid.velocity.x, -_maxFallingSpeed);
                }

            }
        }


        public override void OnStateExit()
        {

        }
    }


}