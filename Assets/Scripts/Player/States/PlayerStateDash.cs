﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    public class PlayerStateDash : PlayerBaseState
    {
        protected float _dashForceX;
        protected float _dashForceY;

        private float _dashHangTime;
        private float _curInDashTime;

        private Vector2 _startPos;
        private Vector2 _curPos;

        private bool _isDashing;
        private bool _dashFinished;

        private float _maxDashTime;
        private float _dashGravityScale;

        protected override bool OnCheckEntryConditions(PlayerInput input, PlayerStatus plStatus, PlayerStateController machine)
        {
            if (CanEnterState == false)
            {
                return false;
            }
            if ( input.DashBtnKeydown && plStatus.CanDash)
            {
                return true;
            }
            return false;
        }

        public override void OnStateEnter()
        {
            base.OnStateEnter();

            _playerParts.Anim.Play(StateName);

            _isDashing = false;
            _dashFinished = false;
            _startPos = new Vector2(_playerParts.Rigid.position.x, _playerParts.Rigid.position.y);

            _playerParts.Rigid.gravityScale = 0;
            _playerParts.Rigid.velocity = Vector2.zero;

            //_playerParts.AnimController.DissolveSet(0.4f,new Color(0.9f,0,4.5f,1));
            _playerParts.AnimController.GlowSet(0.99f, new Color(0.9f, 0, 4.5f, 1));

        }

        public override void ApplySettings()
        {
            StateName = "Dash";
            _dashForceX = _playerParts.Settings.DashForceX;
            _dashForceY = _playerParts.Settings.DashForceY;
            _dashHangTime = _playerParts.Settings.DashHangTime;
            _maxDashTime = _playerParts.Settings.MaxDashTime;
            _dashGravityScale = _playerParts.Settings.DashGravityScale;

            _curInDashTime = 0;
        }

        public override void CheckExitConditions()
        {
            if (_dashFinished)
            {
                if (CheckEntryConditions<PlayerStateMoveAir>(_input, _plStatus, Machine as PlayerStateController))
                {
                    Machine.SetState<PlayerStateMoveAir>();
                    return;
                }
                if (CheckEntryConditions<PlayerStateIdle>(_input, _plStatus, Machine as PlayerStateController))
                {
                    Machine.SetState<PlayerStateIdle>();
                    return;
                }
                if (CheckEntryConditions<PlayerStateRun>(_input, _plStatus, Machine as PlayerStateController))
                {
                    Machine.SetState<PlayerStateRun>();
                    return;
                }
            }
            if (CheckEntryConditions<PlayerStateAxeAttack>(_input, _plStatus, Machine as PlayerStateController))
            {
                Machine.SetState<PlayerStateAxeAttack>();
                return;
            }
        }

        public override void Update()
        {
            base.Update();
            if (ExecuteStateLogic)
            {
                _curPos = new Vector2(_playerParts.Rigid.position.x, _playerParts.Rigid.position.y);

                if (_isDashing)
                {
                    if (_curInDashTime >= _maxDashTime)
                    {
                        _dashFinished = true;
                    }

                    if (_input.DashBtnKeydown)
                    {
                        _dashFinished = true;
                    }

                    if (/*_plStatus.IsGrounded || _plStatus.IsGroundedSlippery ||*/ _plStatus.IsOnLedgeLeft || _plStatus.IsOnLedgeRight || _plStatus.IsOnWallRight || _plStatus.IsOnWallLeft)
                    {
                        _dashFinished = true;
                    }
                }


                if (_input.InputX > 0)
                {
                    _playerParts.PlayerSprite.flipX = false;
                    Quaternion target = Quaternion.Euler(0, 0, 0);
                    _playerParts.PlayerRig.rotation = target;
                }
                else if (_input.InputX < 0)
                {
                    _playerParts.PlayerSprite.flipX = true;
                    Quaternion target = Quaternion.Euler(0, 180, 0);
                    _playerParts.PlayerRig.rotation = target;
                }

                _curInDashTime += Time.deltaTime;

            }
        }

        public override void FixedUpdate()
        {
            if (ExecuteStateLogic)
            {
                if (_isDashing == false && (_curInDashTime >= _dashHangTime || _input.DashBtnKeyup))
                {
                    float velY = 0 /*(_input.InputYRaw < 0 ? 0 : _input.InputYRaw)*/;
                    float velX;

                    if (_input.InputXRaw == 0)
                    {
                        velX = _plStatus.FacesRight() ? 1 : -1;
                    }
                    else
                    {
                        velX = _input.InputXRaw;
                    }

                    Vector3 test = Vector3.Normalize(new Vector3(velX, velY));
                    _playerParts.Rigid.velocity = new Vector2(test.x * _dashForceX, test.y * _dashForceY);

                    _curInDashTime = 0;
                    _isDashing = true;
                }
            }
        }

        public override void OnStateExit()
        {
            _playerParts.Rigid.gravityScale = _playerParts.Settings.GravityScale;

            _playerParts.AnimController.GlowResetSprites();

            _plStatus.CanDash = false;
        }

        void OnDrawGizmos()
        {
            if (DrawStateGizmoz)
            {
                if (_playerParts && Machine && Machine.GetCurrentState is PlayerStateDash)
                {
                    Gizmos.color = Color.red;
                    Gizmos.DrawLine(new Vector3(_playerParts.Rigid.position.x, _playerParts.Rigid.position.y, 0), new Vector3(_playerParts.Rigid.position.x, _playerParts.Rigid.position.y, 0) + Vector3.Normalize(new Vector3(_input.InputX, _input.InputY)) * 2);
                }
            }
        }
    }
}
