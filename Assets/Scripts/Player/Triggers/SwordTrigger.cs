﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    public class SwordTrigger : MonoBehaviour
    {
        public bool HasColissionWithEnemy;
         
        public List<BaseEnemy> EnemiesToProcess;

        public List<LayerMask> FloorLayers;

        private Collider2D _collider;

        void Start()
        {
            _collider = GetComponent<Collider2D>();
        }

        public void Reset()
        {
            EnemiesToProcess.Clear();
        }

        public void OnTriggerEnter2D(Collider2D other)
        {
            BaseEnemy temp = other.GetComponent<BaseEnemy>();

            if (temp && temp is IAttackable)
            {
                if (EnemiesToProcess.Contains(temp) == false)
                {
                    EnemiesToProcess.Add(temp);
                }
            }

            if (FloorLayers.Contains(other.gameObject.layer))
            {
                // todo hit wall effect
            }
        } 
    }
}