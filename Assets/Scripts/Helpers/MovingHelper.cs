﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    public static class MovingHelper
    {
        // returns true if vector was modified
        public static bool MoveRigid2DDamped(Rigidbody2D rigid, ref float curVel, float InputXRaw, float _accelerationTest, float _dampingOnAccelerate, float _dampingOnTurn, float _dampingOnStop)
        {
            curVel = rigid.velocity.x;

            if (Mathf.Abs(InputXRaw) < 0.01f)
            {
                curVel += InputXRaw;
                curVel *= Mathf.Pow(1 - _dampingOnStop, Time.fixedDeltaTime * _accelerationTest);
                rigid.velocity = new Vector2(curVel, rigid.velocity.y);
                return true;
            }
            else if (Mathf.Sign(InputXRaw) != Mathf.Sign(curVel))
            {
                curVel += InputXRaw;
                curVel *= Mathf.Pow(1 - _dampingOnTurn, Time.fixedDeltaTime * _accelerationTest);
                rigid.velocity = new Vector2(curVel, rigid.velocity.y);
                return true;
            }
            else if (Mathf.Abs((curVel + InputXRaw) * Mathf.Pow(1 - _dampingOnAccelerate, Time.deltaTime * _accelerationTest)) > Mathf.Abs(curVel))
            {
                curVel += InputXRaw;
                curVel *= Mathf.Pow(1 - _dampingOnAccelerate, Time.deltaTime * _accelerationTest);
                rigid.velocity = new Vector2(curVel, rigid.velocity.y);
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool MoveRigid2DCustom(Rigidbody2D rigid, float inputX, float moveSpeed, float modAccelerate, float modTurn, float modStop, bool fixOverSpeedOnAccelerate = false)
        {
            //  STOP
            if (Mathf.Abs(inputX) < 0.01f)
            {
                float velX = Mathf.MoveTowards(rigid.velocity.x, 0, modStop * moveSpeed * Time.deltaTime);
                rigid.velocity = new Vector2(velX, rigid.velocity.y);
                return true;
            }
            //  TURN
            else if (Mathf.Sign(inputX) != Mathf.Sign(rigid.velocity.x))
            {
                float velX = Mathf.MoveTowards(rigid.velocity.x, inputX * moveSpeed, modTurn * moveSpeed * Time.deltaTime);
                rigid.velocity = new Vector2(velX, rigid.velocity.y);
                return true;
            }
            //  ACCELERATE
            else
            {
                if (fixOverSpeedOnAccelerate)
                {
                    float velX = Mathf.MoveTowards(rigid.velocity.x, inputX * moveSpeed, modAccelerate * moveSpeed * Time.deltaTime);
                    rigid.velocity = new Vector2(velX, rigid.velocity.y);
                    return true;
                }
                else
                {
                    if (Mathf.Abs(rigid.velocity.x) < moveSpeed)
                    {
                        float velX = Mathf.MoveTowards(rigid.velocity.x, inputX * moveSpeed, modAccelerate * moveSpeed * Time.deltaTime);
                        rigid.velocity = new Vector2(velX, rigid.velocity.y);
                        return true;
                    }
                }
            }
            return false;
        }

        public static bool ClampMoveSpeed()
        {

            return true;
        }
    }
}
