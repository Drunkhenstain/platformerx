﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    public class HighscorePlayerPrefs : MonoBehaviour
    {
        public PlayerStatus plStatus;
        public List<string> Scores;
        public string newScoreValue = null;

        private static string KeyFormat = "PlatformerX_Score_{0}";
        private static string ValueFormat = "Completed after: {0} Deaths: {1}";

        // Start is called before the first frame update
        void Start()
        {
            bool read = true;

            string curCheckKey;
            for (int i = 0; read; i++)
            {
                curCheckKey = string.Format(KeyFormat, i);

                if (PlayerPrefs.HasKey(curCheckKey))
                {
                    //Debug.Log("PlayerPrefs HasKey: " + curCheckKey);
                    string val = PlayerPrefs.GetString(curCheckKey);
                    if (!string.IsNullOrEmpty(val))
                    {
                        Scores.Add(PlayerPrefs.GetString(curCheckKey));
                    }
                    else
                    {
                        PlayerPrefs.DeleteKey(curCheckKey);
                    }
                }
                else
                {
                    read = false;
                }
            }

            if (plStatus == null)
            {
                Debug.LogWarning("HighscorePlayerPrefs: PlayerStats not set");
            }
            
        }

        void Update()
        {
            if (plStatus && plStatus.LevelComplete == true && string.IsNullOrEmpty(newScoreValue))
            {
                newScoreValue = string.Format(ValueFormat, Time.time, plStatus.DeathCount);
            }
        }

        void OnApplicationQuit()
        {
            if (!string.IsNullOrEmpty(newScoreValue))
            {
                string newScoreKey = string.Format(KeyFormat, Scores.Count);

                PlayerPrefs.SetString(newScoreKey, newScoreValue);
                Debug.Log(newScoreValue);
            }
        }
    }
}