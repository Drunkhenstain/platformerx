﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class PIDControlloer : MonoBehaviour
{
    float target = 90f;
    float current = 5;

    float targetCalced = 0;

    float pFactor = 0.9f;
    float iFactor = 0.5f;

    float accumulator;

    int iterations = 0;

    // Start is called before the first frame update
    void Start()
    {

    }

    void AddFactor()
    {
        float perc90 = target * pFactor;
        float factor = perc90 / current;

        current *= factor;
    }

    void Integrate()
    {
        float difference = target - current;
        accumulator += (math.abs(difference)) * iFactor;

        if (difference >= 0)
            targetCalced += accumulator;
        else targetCalced -= accumulator;
    }

    void LogValues()
    {
        Debug.Log(iterations + "Current:\t" + current);
        Debug.Log(iterations + "Accu   :\t" + accumulator);
        Debug.Log(iterations + "TrgCalc:\t" + targetCalced);
    }

    void FixedUpdate()
    {
        // STEP 1
        AddFactor();
        Integrate();
        LogValues();

        iterations += 1;
    }
}

