﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    [RequireComponent(typeof(CinemachineImpulseSource))]
    public class GlobalEffectsHelper : MonoBehaviour
    {
        private CinemachineImpulseSource _camShaker;

        void Start()
        {
            _camShaker = GetComponent<CinemachineImpulseSource>();
        }

        public void ShakeCam()
        {
            if (_camShaker)
            {
                _camShaker.GenerateImpulse();
            }
        }

        private void Update()
        {
            //if()
        }

        public void SetSlowMo(float timeScale, float length, bool fadeBack = false)
        {
            _slowMoCoRoutine = SlowMoCoroutine(timeScale, length, fadeBack);
            StartCoroutine(_slowMoCoRoutine);
        }

        private IEnumerator _slowMoCoRoutine;
        private static IEnumerator SlowMoCoroutine(float timeScale, float length, bool fadeBack)
        {
            timeScale = timeScale > 0.001f ? timeScale : 0.001f;
            Time.timeScale = timeScale;

            if (fadeBack)
            { 
            }
            else
            {
                yield return new WaitForSeconds(timeScale * length);
                Time.timeScale = 1f;
            }
        }
    }
}