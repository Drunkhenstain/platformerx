﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    public static class StaticChecks
    {
        public static PlayerStateController GetPlayer(GameObject obj) 
        {
            var _tempPlayer = obj.GetComponent<PlayerStateController>();
            //if (_tempPlayer == null)
            //{
            //    _tempPlayer = obj.GetComponentInParent<PlayerStateController>();
            //}
            if (_tempPlayer)
            {
                return _tempPlayer;
            }

            return null;
        }
        public static bool IsPlayer(GameObject obj)
        {
            var _tempPlayer = obj.GetComponent<PlayerStateController>();
            //if (_tempPlayer == null)
            //{
            //    _tempPlayer = obj.GetComponentInParent<PlayerStateController>();
            //}
            if (_tempPlayer)
            {
                return true;
            }

            return false;
        }
    } 
}