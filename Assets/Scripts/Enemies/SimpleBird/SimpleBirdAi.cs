﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Rendering;
using UnityEngine;

namespace PlatformerX
{
    public class SimpleBirdAi : BaseEnemy, IAttackable
    {
        public bool HasColissionWithPlayer;
        public bool PlayerCanJumpOn;
        public bool ColissionFromAbove;

        public float PointsOnKill;
        public float Damage;
        public float Speed;
        public float MaxRangeX;
        public float DieAnimLength;
        public float ImmuntyTime;
        public Vector2 PushPlayerOnContactVel;

        public LayerMask FloorLayer;
        public LayerMask DeadlyFloorLayer;
        public LayerMask SlippyFloorLayer;

        private float _startPosX;
        private float _curImmuntyTime;

        private float _dieAnimDeltaVal;
        private float _dieColorAlphaDeltaVal;

        private PlayerStateController _tempPlayer;
        private PlayerStateController Player;

        private ParticleSystem _bloodEffect;
        private Collider2D _collider;
        private Rigidbody2D _rigid;

        private Animator _anim;
        private SpriteRenderer _sprite;
        private GlobalEffectsHelper _globalEffects;
        private GlobalBlackboard _globalBlackboard;

        private Vector2 _bottomLeftPos;
        private Vector2 _bottomRightPos;
        private Vector2 _bottomMidPos;
        private Vector2 _midLeftPos;
        private Vector2 _midRightPos;
        private Vector2 _topLeftPos;
        private Vector2 _topRightPos;

        private bool _flyRight = true;
        private bool _doFly = true;
        private bool _doDie = false;

        void Start()
        {
            if (_globalBlackboard == null)
            {
                _globalBlackboard = GameObject.FindObjectOfType<GlobalBlackboard>();
            }

            if (_globalEffects == null)
            {
                _globalEffects = GameObject.FindObjectOfType<GlobalEffectsHelper>();
            }
            _bloodEffect = GetComponentInChildren<ParticleSystem>();
            _sprite = GetComponent<SpriteRenderer>();
            _collider = GetComponent<Collider2D>();
            _rigid = GetComponent<Rigidbody2D>();
            _anim = GetComponent<Animator>();

            _startPosX = _rigid.position.x;

            _dieAnimDeltaVal = 1 / DieAnimLength;
            _dieColorAlphaDeltaVal = _sprite.color.a / DieAnimLength;
            _curImmuntyTime = 0;

            if (BaseHealth == 0)
            {
                BaseHealth = 1;
            }
            CurrentHealth = BaseHealth;

            CalcRaycastPositions();
        }
        void CalcRaycastPositions()
        {
            Bounds bounds = _collider.bounds;
            bounds.Expand(-0.01f);

            _bottomLeftPos = new Vector2(bounds.min.x, bounds.min.y);
            _bottomRightPos = new Vector2(bounds.max.x, bounds.min.y);
            _bottomMidPos = new Vector2(bounds.center.x, bounds.min.y);

            _midLeftPos = new Vector2(bounds.min.x, bounds.center.y);
            _midRightPos = new Vector2(bounds.max.x, bounds.center.y);

            _topLeftPos = new Vector2(bounds.min.x, bounds.max.y);
            _topRightPos = new Vector2(bounds.max.x, bounds.max.y);
        }

        void Update()
        {
            CalcRaycastPositions();

            if (_curImmuntyTime > 0)
            {
                _curImmuntyTime -= Time.deltaTime;
            }
            else
            {
                if (HasColissionWithPlayer)
                {
                    if (PlayerCanJumpOn && ColissionFromAbove)
                    {
                        OnWeaponAttack(1, false);
                        Player.OnSimpleBirdTopJump(12);
                    }
                    else
                    {
                        Player.OnSimpleBirdContact(Damage, PushPlayerOnContactVel);
                        Reset();
                    }
                }
            }


            if (_doFly)
            {
                if (_rigid.position.x - _startPosX > MaxRangeX)
                {
                    _flyRight = false;
                }
                else if (_rigid.position.x - _startPosX < -MaxRangeX)
                {
                    _flyRight = true;
                }

                CheckWalls();

                if (_flyRight)
                {
                    _sprite.flipX = false;
                }
                else
                {
                    _sprite.flipX = true;
                }
            }

            if (_doDie)
            {
                _rigid.SetRotation(_rigid.rotation + 1080 * Time.deltaTime);
                _sprite.color = new Color(_sprite.color.r, _sprite.color.g, _sprite.color.b, Mathf.MoveTowards(_sprite.color.a, 0, _dieColorAlphaDeltaVal * Time.deltaTime));
                _anim.speed = Mathf.MoveTowards(_anim.speed, 0, _dieAnimDeltaVal * Time.deltaTime);
            }
        }

        void FixedUpdate()
        {
            if (_doFly)
            {
                if (_flyRight)
                {
                    _rigid.velocity = new Vector2(Speed, 0);
                }
                else
                {
                    _rigid.velocity = new Vector2(-Speed, 0);
                }
            }
        }

        void CheckWalls()
        {
            if (RaycastAllLayers(_midLeftPos, Vector2.left, 0.15f)
                || RaycastAllLayers(_topLeftPos, Vector2.left, 0.15f)
                || RaycastAllLayers(_bottomLeftPos, Vector2.left, 0.15f))
            {
                _flyRight = true;
            }

            if (RaycastAllLayers(_midRightPos, Vector2.right, 0.15f)
                || RaycastAllLayers(_topRightPos, Vector2.right, 0.15f)
                || RaycastAllLayers(_bottomRightPos, Vector2.right, 0.15f))
            {
                _flyRight = false;
            }
        }

        private bool RaycastLayer(Vector2 pos, Vector2 dir, float dist, LayerMask layer)
        {
            return Physics2D.Raycast(pos, dir, dist, layer);
        }

        private bool RaycastAllLayers(Vector2 pos, Vector2 dir, float dist)
        {
            if (RaycastLayer(pos, dir, dist, DeadlyFloorLayer)
                || RaycastLayer(pos, dir, dist, FloorLayer)
                || RaycastLayer(pos, dir, dist, SlippyFloorLayer))
            {
                Debug.Log("SimpleBird Floor contact");
                return true;
            }

            return false;
        }

        public void Reset()
        {
            HasColissionWithPlayer = false;
            //Player = null;
        }

        public void OnCollisionEnter2D(Collision2D other)
        {
            _tempPlayer = StaticChecks.GetPlayer(other.gameObject);

            if (_tempPlayer != null)
            {
                Player = _tempPlayer;
                HasColissionWithPlayer = true;
                ColissionFromAbove = false;

                if (PlayerCanJumpOn)
                {
                    if (other.GetContact(0).point.y >= _topLeftPos.y - 0.1f)
                    {
                        if (other.rigidbody.velocity.y < 0)
                        {
                            ColissionFromAbove = true;
                        }
                    } 
                }
            }
        }
        public void OnCollisionExit2D(Collision2D other)
        {
            _tempPlayer = StaticChecks.GetPlayer(other.gameObject); 
            if (_tempPlayer != null)
            {
                HasColissionWithPlayer = false;
            }
        }

        public bool OnWeaponAttack(float damage, bool fromRight)
        {
            if (_curImmuntyTime <= 0)
            {
                CurrentHealth -= damage;
                _bloodEffect.Play();

                _curImmuntyTime = ImmuntyTime;

                if (fromRight)
                {
                    _rigid.velocity = new Vector2(-3, _rigid.velocity.y);
                }
                else
                {
                    _rigid.velocity = new Vector2(3, _rigid.velocity.y);
                }

                if (CurrentHealth <= 0)
                {
                    Die();
                }

                return true;
            }
            return false;
        }

        private void Die()
        {
            _doDie = true;
            _rigid.gravityScale = 2f;
            _doFly = false;
            _collider.enabled = false;
            _globalBlackboard.EnemyDied(PointsOnKill);
            _globalBlackboard.Player.GetHealth(UnityEngine.Random.Range(1, PlayerHealthOnDeath));

            GameObject.Destroy(gameObject, DieAnimLength);
        }
    }
}