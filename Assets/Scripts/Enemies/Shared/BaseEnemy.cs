﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    public class BaseEnemy : MonoBehaviour
    {
        public float PlayerHealthOnDeath;

        public float BaseHealth;
        public float CurrentHealth;
        public void ResetHealth()
        {
            CurrentHealth = BaseHealth;
        }
    }
}