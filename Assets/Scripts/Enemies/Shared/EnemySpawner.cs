﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    public class EnemySpawner : MonoBehaviour
    {
        public GameObject Prefab;
        public GameObject Enemy;

        private Vector3 _startPos;
        private PlayerStateController _player;

        void Start()
        {
            _player = FindObjectOfType<PlayerStateController>();
             
            _startPos = Enemy.transform.position;
        }

        void Update()
        {
            if (_player.GetCurrentState is PlayerStateReset)
            {
                if (Enemy == null)
                {
                    Enemy = GameObject.Instantiate(Prefab, _startPos, Quaternion.identity, gameObject.transform);
                }
                else
                {
                    var temp = Enemy.GetComponent<BaseEnemy>();

                    if (temp)
                    {
                        temp.ResetHealth();
                    }
                }
            }
        }

        public void OnDrawGizmoz()
        { 
            Gizmos.color = Color.yellow;
            Gizmos.DrawSphere(transform.position, 3f);
        }
    }
}
