﻿
namespace PlatformerX
{
    public interface IAttackable
    {
        bool OnWeaponAttack(float damage, bool fromRight);
    }
}