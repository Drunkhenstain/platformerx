﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    public class MechaGoombaParts : MonoBehaviour
    {
        public void Start()
        {
            if (GlobalBlackboard == null)
            {
                GlobalBlackboard = GameObject.FindObjectOfType<GlobalBlackboard>();
            }

            if (GlobalEffects == null)
            {
                GlobalEffects = GameObject.FindObjectOfType<GlobalEffectsHelper>();
            }
        }

        public Transform Rig;

        public Rigidbody2D Rigid;

        public Animator Anim;

        public MechaGoombaAI AI;

        public MechaGoombaStateController StateMachine;

        public MechaGoombaSettings Settings;

        public Collider2D Collider;

        public ParticleSystem BloodEffect;

        public MechaGoombaJumpTriggerTop JumpTriggerTop;
        //public MechaGoombaJumpTriggerBottom JumpTriggerBottom;

        public MechaGoombaTriggerSide SideTriggerFront;
        public MechaGoombaTriggerSide SideTriggerBack;

        public List<SpriteRenderer> BodySprites;

        public GlobalEffectsHelper GlobalEffects;
        public GlobalBlackboard GlobalBlackboard;
    }
}