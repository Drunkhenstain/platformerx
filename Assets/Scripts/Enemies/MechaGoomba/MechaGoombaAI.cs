﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    [RequireComponent(typeof(MechaGoombaParts))]
    public class MechaGoombaAI : BaseEnemy, IAttackable
    {
        private MechaGoombaParts _parts;

        private bool _facesRight;
        private bool _playerIsRight;
        private bool _canLookBack;

        private bool _isInHeavyAttack;

        public float InputX;

        private float _lookDistX;
        private Vector2 _bottomLeftPos;
        private Vector2 _bottomRightPos;
        private Vector2 _bottomMidPos;
        private Vector2 _midLeftPos;
        private Vector2 _midRightPos;
        private Vector2 _topLeftPos;
        private Vector2 _topRightPos;

        private Collider2D _collider;

        private LayerMask _curGroundLayer;


        public void GetDamage(float dmg) { CurrentHealth -= dmg; }
        public float GetHealth { get { return CurrentHealth; } }

        // Start is called before the first frame update
        void Start()
        {
            if (!_parts)
            {
                _parts = GetComponent<MechaGoombaParts>();
            }

            _parts.StateMachine.SetState<MechaGoombaStateIdle>();

            _collider = _parts.Collider;

            InputX = 1;

            foreach (SpriteRenderer sprite in _parts.BodySprites)
            {
                sprite.color = _parts.Settings.GoombaColor;
            }

            _parts.BloodEffect.startColor = _parts.Settings.GoombaColor;

            CalcRaycastPositions();
            _curGroundLayer = _parts.Settings.GroundLayer;
            _canLookBack = _parts.Settings.CanLookBack;

            BaseHealth = _parts.Settings.Health;
            CurrentHealth = BaseHealth;
            _lookDistX = _parts.Settings.LookDistanceX;

            CheckCurrentGround();
        }

        void CalcRaycastPositions()
        {
            Bounds bounds = _collider.bounds;
            bounds.Expand(-0.01f);

            _bottomLeftPos = new Vector2(bounds.min.x, bounds.min.y);
            _bottomRightPos = new Vector2(bounds.max.x, bounds.min.y);
            _bottomMidPos = new Vector2(bounds.center.x, bounds.min.y);

            _midLeftPos = new Vector2(bounds.min.x, bounds.center.y);
            _midRightPos = new Vector2(bounds.max.x, bounds.center.y);

            _topLeftPos = new Vector2(bounds.min.x, bounds.max.y);
            _topRightPos = new Vector2(bounds.max.x, bounds.max.y);
        }


        // Update is called once per frame
        void Update()
        {
            CheckCurrentGround();

            if (_parts.StateMachine.GetCurrentState is MechaGoombaStateIdle == false)
            {
                RotateRig();
            }

            CalcRaycastPositions();

            if (_parts.StateMachine.GetCurrentState is MechaGoombaStateLieOnBack == false && _parts.StateMachine.GetCurrentState is MechaGoombaStateGetHit == false)
            {
                if (CheckJumpTopTrigger())
                {
                    CurrentHealth -= 1;
                    _parts.BloodEffect.Play();
                    _parts.StateMachine.SetState<MechaGoombaStateLieOnBack>();
                    return;
                }
                else if (CheckSideTriggers())
                {
                    _parts.StateMachine.SetState<MechaGoombaStateWalk>();
                    return;
                }

                if (_parts.StateMachine.GetCurrentState is MechaGoombaStateWalk == true || _parts.StateMachine.GetCurrentState is MechaGoombaStateIdle)
                {
                    if (CheckPlayerInRange())
                    {
                        if (_playerIsRight)
                        {
                            if (_facesRight == false)
                            {
                                InputX = 1;
                                RotateRig();
                            }
                        }
                        else
                        {
                            if (_facesRight)
                            {
                                InputX = -1;
                                RotateRig();
                            }
                        }
                        _parts.StateMachine.SetState<MechaGoombaStateAttack>();
                        return;
                    }
                }
            }

            if (_parts.StateMachine.GetCurrentState is MechaGoombaStateWalk)
            {
                if (CheckWallHolesInDirection())
                {
                    //Debug.Log("Goomba checks hole and wall");
                    _parts.StateMachine.SetState<MechaGoombaStateIdle>();
                    return;
                }
            }
        }

        void RotateRig()
        {
            if (InputX < 0)
            {
                Quaternion target = Quaternion.Euler(0, 180, _parts.Rig.rotation.z);
                _parts.Rig.rotation = target;
                _facesRight = false;
            }
            else
            {
                Quaternion target = Quaternion.Euler(0, 0, _parts.Rig.rotation.z);
                _parts.Rig.rotation = target;
                _facesRight = true;
            }
        }

        bool CheckPlayerInRange()
        {
            if (_canLookBack)
            {
                if (RaycastLayer(_topRightPos, Vector2.right, _lookDistX, _parts.Settings.PlayerLayer) == true || RaycastLayer(_midRightPos, Vector2.right, _lookDistX, _parts.Settings.PlayerLayer) == true)
                {
                    _playerIsRight = true;
                    return true;
                }
                else
                {
                    _playerIsRight = false;
                }

                if (RaycastLayer(_topLeftPos, Vector2.left, _lookDistX, _parts.Settings.PlayerLayer) == true || RaycastLayer(_midLeftPos, Vector2.left, _lookDistX, _parts.Settings.PlayerLayer) == true)
                {
                    _playerIsRight = false;
                    return true;
                }
            }
            else
            {
                if (_facesRight)
                {
                    if (RaycastLayer(_topRightPos, Vector2.right, _lookDistX, _parts.Settings.PlayerLayer) == true || RaycastLayer(_midRightPos, Vector2.right, _lookDistX, _parts.Settings.PlayerLayer) == true)
                    {
                        _playerIsRight = true;
                        return true;
                    }
                    else
                    {
                        _playerIsRight = false;
                    }
                }
                else
                {
                    if (RaycastLayer(_topLeftPos, Vector2.left, _lookDistX, _parts.Settings.PlayerLayer) == true || RaycastLayer(_midLeftPos, Vector2.left, _lookDistX, _parts.Settings.PlayerLayer) == true)
                    {
                        _playerIsRight = false;
                        return true;
                    }
                }
            }
            return false;
        }

        bool CheckJumpTopTrigger()
        {
            if (_parts.JumpTriggerTop.HasColissionWithPlayer && _parts.JumpTriggerTop.Player != null)
            {
                _parts.JumpTriggerTop.Player.OnGoombaTopJump(_parts.Settings.PushPlayerOnTopJumpVelocity);
                _parts.JumpTriggerTop.Reset();
                return true;
            }
            return false;
        }

        bool CheckSideTriggers()
        {
            if (_parts.SideTriggerFront.HasColissionWithPlayer)
            {
                bool goombaIsLeft = _facesRight ? true : false;
                Vector2 pushForce;
                float damage;

                if (_isInHeavyAttack)
                {
                    pushForce = _parts.Settings.PushPlayerOnAttackHitVelocity;
                    damage = _parts.Settings.HitDamage;
                }
                else
                {
                    pushForce = _parts.Settings.PushPlayerOnTouchVelocity;
                    damage = _parts.Settings.TouchDamage;
                }

                if (goombaIsLeft == false)
                {
                    pushForce.x *= -1;
                }

                if (_parts.SideTriggerFront.Player)
                {
                    _parts.SideTriggerFront.Player.OnGoombaAttackSide(damage, pushForce);
                }
                _parts.SideTriggerFront.Reset();
                _parts.SideTriggerBack.Reset();
                return true;
            }
            else if (_parts.SideTriggerBack.HasColissionWithPlayer)
            {

                bool goombaIsLeft = _facesRight ? false : true;

                Vector2 pushForce = _parts.Settings.PushPlayerOnTouchVelocity;
                float damage = _parts.Settings.TouchDamage;

                if (goombaIsLeft == false)
                {
                    pushForce.x *= -1;
                }

                if (_parts.SideTriggerBack.Player)
                {
                    _parts.SideTriggerBack.Player.OnGoombaAttackSide(damage, pushForce);
                }
                _parts.SideTriggerFront.Reset();
                _parts.SideTriggerBack.Reset();
                InputX *= -1;
                RotateRig();
                return true;
            }
            return false;
        }

        void CheckCurrentGround()
        {
            // check hole
            if (RaycastLayer(_bottomMidPos, Vector2.down, 0.15f, _parts.Settings.GroundLayer) == true)
            {
                _curGroundLayer = _parts.Settings.GroundLayer;
            }
            else if (RaycastLayer(_bottomMidPos, Vector2.down, 0.15f, _parts.Settings.SlippyGroundLayer) == true)
            {
                _curGroundLayer = _parts.Settings.SlippyGroundLayer;
            }
            else if (RaycastLayer(_bottomMidPos, Vector2.down, 0.15f, _parts.Settings.DeadlyGroundLayer) == true)
            {
                _curGroundLayer = _parts.Settings.DeadlyGroundLayer;
            }
        }


        public bool OnWeaponAttack(float damage, bool fromRight)
        {
            if (_parts.StateMachine.GetCurrentState is MechaGoombaStateLieOnBack == false)
            {
                _parts.BloodEffect.Play();
                CurrentHealth -= damage;

                if (CurrentHealth <= 1)
                {
                    _parts.StateMachine.SetState<MechaGoombaStateLieOnBack>();
                }
                else
                {
                    if (_parts.StateMachine.GetCurrentState is MechaGoombaStateGetHit)
                    {
                        _parts.StateMachine.SetState<MechaGoombaStateGetHit2>();
                    }
                    else if (_parts.StateMachine.GetCurrentState is MechaGoombaStateGetHit2)
                    {
                        _parts.StateMachine.SetState<MechaGoombaStateLieOnBack>();
                    }
                    //_parts.StateMachine.SetState<MechaGoombaBaseState>();
                }

                if (fromRight)
                {
                    _parts.Rigid.velocity = new Vector2(-3, _parts.Rigid.velocity.y);
                }
                else
                {
                    _parts.Rigid.velocity = new Vector2(3, _parts.Rigid.velocity.y);
                }
                //_parts.GlobalEffects.ShakeCam();
                //_parts.GlobalEffects.SetSlowMo(0.2f, 0.1f);
                return true;
            }
            else
            {
                if (fromRight)
                {
                    _parts.Rigid.velocity = new Vector2(-3, _parts.Rigid.velocity.y);
                }
                else
                {
                    _parts.Rigid.velocity = new Vector2(3, _parts.Rigid.velocity.y);
                }
                //_parts.GlobalEffects.ShakeCam();
                //_parts.GlobalEffects.SetSlowMo(0.2f, 0.1f);
                _parts.BloodEffect.Play();
                CurrentHealth -= damage;

                if (CurrentHealth <= 0)
                {
                    TriggerBottomPlayerJump();
                }
                return true;
            } 
        }

        private bool RaycastLayer(Vector2 pos, Vector2 dir, float dist, LayerMask layer)
        {
            return Physics2D.Raycast(pos, dir, dist, layer);
        }

        private bool RaycastAllLayers(Vector2 pos, Vector2 dir, float dist)
        {
            if (RaycastLayer(pos, dir, dist, _parts.Settings.DeadlyGroundLayer)
                || RaycastLayer(pos, dir, dist, _parts.Settings.GroundLayer)
                || RaycastLayer(pos, dir, dist, _parts.Settings.SlippyGroundLayer))
            {
                return true;
            }

            return false;
        }


        public bool CheckWallHolesInDirection(bool setInputX = true)
        {
            //right
            if (InputX == 1)
            {
                // check wall
                if (RaycastAllLayers(_midRightPos, Vector2.right, 0.15f) == true || RaycastAllLayers(_bottomRightPos, Vector2.right, 0.15f) == true)
                {
                    if (setInputX)
                    {
                        InputX = -1;
                    }
                    return true;
                }

                // check hole
                if (RaycastAllLayers(_bottomRightPos, Vector2.down, 0.15f) == false)
                {
                    if (setInputX)
                    {
                        InputX = -1;
                    }
                    return true;
                }
            }

            //left
            if (InputX == -1)
            {
                // check wall
                if (RaycastAllLayers(_midLeftPos, Vector2.left, 0.15f) == true|| RaycastAllLayers(_bottomLeftPos, Vector2.left, 0.15f) == true)
                {
                    if (setInputX)
                    {
                        InputX = 1;
                    }
                    return true;
                }
                else if (RaycastAllLayers(_midLeftPos, Vector2.left, 0.15f) == true)
                {
                    if (setInputX)
                    {
                        InputX = 1;
                    }
                    return true;
                }

                // check hole
                if (RaycastAllLayers(_bottomLeftPos, Vector2.down, 0.15f) == false)
                {
                    if (setInputX)
                    {
                        InputX = 1;
                    }
                    return true;
                }
            }
            return false;
        }

        public void TriggerHeavyAttackStatusOn()
        {
            _isInHeavyAttack = true;
        }
        public void TriggerHeavyAttackStatusOff()
        {
            _isInHeavyAttack = false;
        }

        internal void TriggerAttackFinish()
        {
            _parts.StateMachine.SetState<MechaGoombaStateIdle>();
        }

        public void TriggerHurtFinish()
        {
            _parts.StateMachine.SetState<MechaGoombaStateIdle>();
        }
        public void TriggerHitFinish()
        {
            _parts.StateMachine.SetState<MechaGoombaStateWalk>();
        }

        public void TriggerBottomPlayerJump()
        {
            _parts.StateMachine.SetCurrentStateLogic(false);
            _parts.Anim.Play("Squash");
            _parts.Rigid.gravityScale = 0.1f;
            _parts.GlobalBlackboard.EnemyDied(_parts.Settings.PointsOnKill);
            _parts.GlobalBlackboard.Player.GetHealth(UnityEngine.Random.Range(1, PlayerHealthOnDeath));
            GameObject.Destroy(gameObject, 1);
        }


        public void TriggerIdleFinish()
        {
            _parts.StateMachine.SetState<MechaGoombaStateWalk>();
        }
         
        //void OnDrawGizmos()
        //{

        //    Gizmos.color = Color.green;
        //    Gizmos.DrawLine(_bottomLeftPos, _bottomLeftPos + (Vector2.down * 0.15f));
        //    Gizmos.DrawLine(_bottomRightPos, _bottomRightPos + (Vector2.down * 0.15f));
        //    Gizmos.color = Color.yellow;
        //    Gizmos.DrawLine(_midLeftPos, _midLeftPos + (Vector2.left * 0.15f));
        //    Gizmos.DrawLine(_midRightPos, _midRightPos + (Vector2.right * 0.15f));

        //    Gizmos.color = Color.red;
        //    Gizmos.DrawLine(_midLeftPos + new Vector2(0, 0.05f), _midLeftPos + (Vector2.left) + new Vector2(0, 0.05f));
        //    Gizmos.DrawLine(_midRightPos + new Vector2(0, 0.05f), _midRightPos + (Vector2.right) + new Vector2(0, 0.05f));
        //}
    }
}
