﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    public class MechaGoombaJumpTriggerTop : MonoBehaviour
    {
        public bool HasColissionWithPlayer;

        private PlayerStateController _tempPlayer;
        public PlayerStateController Player;

        private Collider2D _collider;

        void Start()
        {
            _collider = GetComponent<Collider2D>();
        }

        public void Reset()
        {
            HasColissionWithPlayer = false;
        }

        public void OnTriggerEnter2D(Collider2D other)
        {
            _tempPlayer = StaticChecks.GetPlayer(other.gameObject);
            
            if (_tempPlayer != null)
            {
                Player = _tempPlayer;
                if (other.bounds.center.y > _collider.bounds.center.y + (other.bounds.size.y / 2.1f))
                {
                    HasColissionWithPlayer = true; 
                } 
            }
        }
        public void OnTriggerExit2D(Collider2D other)
        {
            _tempPlayer = StaticChecks.GetPlayer(other.gameObject);

            if (_tempPlayer != null)
            {
                HasColissionWithPlayer = false;
            }
        }
    }
}