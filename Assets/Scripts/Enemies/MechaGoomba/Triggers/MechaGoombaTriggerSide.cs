﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

namespace PlatformerX
{
    public class MechaGoombaTriggerSide : MonoBehaviour
    {
        public bool IsFront;

        public bool HasColissionWithPlayer;

        private PlayerStateController _tempPlayer;
        public PlayerStateController Player;

        private Collider2D _collider;

        void Start()
        {
            _collider = GetComponent<Collider2D>();
        }


        public void Reset()
        {
            HasColissionWithPlayer = false;
            //Player = null;
        }

        public void OnTriggerEnter2D(Collider2D other)
        {
            _tempPlayer = StaticChecks.GetPlayer(other.gameObject);
            if (_tempPlayer != null)
            {
                Player = _tempPlayer;
                HasColissionWithPlayer = true;
            }
        }
        public void OnTriggerExit2D(Collider2D other)
        {
            _tempPlayer = StaticChecks.GetPlayer(other.gameObject);
            if (_tempPlayer != null)
            {
                HasColissionWithPlayer = false;
            }
        }
    }
}