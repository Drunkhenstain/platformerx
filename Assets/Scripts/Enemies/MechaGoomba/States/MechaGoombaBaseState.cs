﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    [RequireComponent(typeof(MechaGoombaParts))]
    public abstract class MechaGoombaBaseState : EnemyBaseState
    {
        public static bool CheckEntryConditions<StateType>(MechaGoombaParts parts, MechaGoombaStateController machine) where StateType : State
        {
            MechaGoombaBaseState targetStateToCheck = (machine.GetState<StateType>() as MechaGoombaBaseState);

            if (targetStateToCheck == null)
            {
                //Debug.Log("targetStateToCheck not found");
                return false;
            }

            return targetStateToCheck.OnCheckEntryConditions(parts, machine);
        }
        protected abstract bool OnCheckEntryConditions(MechaGoombaParts parts, MechaGoombaStateController machine);


        protected MechaGoombaParts _parts;

        public override void OnStateEnter()
        {
            if (!_parts)
            {
                _parts = GetComponent<MechaGoombaParts>();
            }

            ApplySettings();
        }

        public override void OnStateInitialize()
        {
        }
    }
}