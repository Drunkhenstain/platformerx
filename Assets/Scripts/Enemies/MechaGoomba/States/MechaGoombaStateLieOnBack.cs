﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    public class MechaGoombaStateLieOnBack : MechaGoombaBaseState
    {
        [SerializeField]
        private float _maxHurtTime;

        [SerializeField]
        private float _inHurtTime;

        public override void OnStateEnter()
        {
            base.OnStateEnter();
            _parts.Anim.Play("Hurt");
            _inHurtTime = 0;
             
        }
        public override void ApplySettings()
        {
            StateName = "Hurt";
            _maxHurtTime = _parts.Settings.HurtTime;
        }

        protected override bool OnCheckEntryConditions(MechaGoombaParts parts, MechaGoombaStateController machine)
        {
            return true;
        }

        public override void CheckExitConditions()
        {
        }

        public override void Update()
        {
            if (ExecuteStateLogic)
            {
                if (_inHurtTime >= _maxHurtTime)
                {
                    _parts.AI.TriggerHurtFinish();
                }
                else
                {
                    if (_parts.JumpTriggerTop.HasColissionWithPlayer)
                    {
                        _parts.AI.GetDamage(1);

                        _parts.JumpTriggerTop.Player.OnGoombaTopJump(_parts.Settings.PushPlayerOnTopJumpVelocity);
                        _parts.JumpTriggerTop.Reset();
                        _parts.BloodEffect.Play();

                        if (_parts.AI.GetHealth <= 0)
                        {
                            _parts.AI.TriggerBottomPlayerJump();
                            ExecuteStateLogic = false;
                        }
                        //if (_parts.AI.Health >= 3)
                        //{
                        //    _parts.AI.Health -= 2;
                        //    _parts.JumpTriggerTop.Player.OnGoombaTopJump(_parts.Settings.PushPlayerOnTopJumpVelocity);
                        //    _parts.JumpTriggerTop.Reset();
                        //}
                        //else
                        //{
                        //    _parts.AI.Health -= 2;
                        //    _parts.JumpTriggerTop.Player.OnGoombaTopJump(_parts.Settings.PushPlayerOnTopJumpVelocity);
                        //    _parts.JumpTriggerTop.Reset();

                        //    _parts.BloodEffect.Play();
                        //    _parts.AI.TriggerBottomPlayerJump();
                        //    ExecuteStateLogic = false;
                        //}
                    }

                    _inHurtTime += Time.deltaTime;
                }
            }
        }

        public override void FixedUpdate()
        {

        }


        public override void OnStateExit()
        {

        }

    }
}