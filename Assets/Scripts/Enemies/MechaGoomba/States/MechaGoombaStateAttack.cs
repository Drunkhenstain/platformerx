﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    public class MechaGoombaStateAttack : MechaGoombaBaseState
    {
        [SerializeField]
        private float _maxAttackTime;

        [SerializeField]
        private float _inAttackTime;

        private float _extraAttackStepX;
        private float _extraAttackDistance;

        public override void OnStateEnter()
        {
            base.OnStateEnter();
            _parts.Anim.Play("PrepareAttack");

            _parts.Anim.speed = 1 / _maxAttackTime;
            _inAttackTime = 0;

            if (_parts.AI.CheckWallHolesInDirection(false) == false)
            {
                //_extraAttackStepX = _extraAttackDistance / _maxAttackTime;
                _extraAttackStepX = _extraAttackDistance / (_maxAttackTime / 2);
            }
            else
            {
                _extraAttackStepX = 0;
            }
        }
        public override void ApplySettings()
        {
            StateName = "Attack";
            _maxAttackTime = _parts.Settings.AttackTime;
            _extraAttackDistance = _parts.Settings.AttackExtraXDistance;
        }

        protected override bool OnCheckEntryConditions(MechaGoombaParts parts, MechaGoombaStateController machine)
        {
            return true;
        }

        public override void CheckExitConditions()
        {

        }

        public override void Update()
        {
            if (ExecuteStateLogic)
            {

                if (_inAttackTime >= _maxAttackTime)
                {
                    _parts.AI.TriggerAttackFinish();
                }
                else
                {

                    _inAttackTime += Time.deltaTime;
                }
            }
        }

        public override void FixedUpdate()
        {
            if (ExecuteStateLogic)
            {
                //if (_parts.AI.CheckWallHolesInDirection(false) == false)
                //{
                //    float newX = _parts.Rigid.position.x + (Mathf.Sign(_parts.AI.InputX) * _extraAttackStepX * Time.fixedDeltaTime);

                //    _parts.Rigid.position = new Vector2(newX, _parts.Rigid.position.y);
                //}

                if (_parts.AI.CheckWallHolesInDirection(false) == false && _inAttackTime > (_maxAttackTime / 2))
                {
                    float newX = _parts.Rigid.position.x + (Mathf.Sign(_parts.AI.InputX) * _extraAttackStepX * Time.fixedDeltaTime);

                    _parts.Rigid.position = new Vector2(newX, _parts.Rigid.position.y);
                }
                
            }
        }


        public override void OnStateExit()
        {
            _parts.Anim.speed = 1;
            _parts.AI.TriggerHeavyAttackStatusOff();
        }

    }
}