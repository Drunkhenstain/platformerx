﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    public class MechaGoombaStateIdle : MechaGoombaBaseState
    {
        [SerializeField]
        private float _maxIdleTime;

        [SerializeField]
        private float _inIdleTime;

        public override void OnStateEnter()
        {
            base.OnStateEnter();
            _parts.Anim.Play("Idle");
            _inIdleTime = 0;
            _parts.Rigid.velocity = new Vector2(0, 0);
        }
        public override void ApplySettings()
        {
            StateName = "Idle";
            _maxIdleTime = _parts.Settings.IdleTime;
        }

        protected override bool OnCheckEntryConditions(MechaGoombaParts parts, MechaGoombaStateController machine)
        {
            return true;
        }

        public override void CheckExitConditions()
        {
        }

        public override void Update()
        {
            if (ExecuteStateLogic)
            {
                if (_parts.AI.InputX < 0)
                {

                }
                else
                {

                }

                if (_inIdleTime >= _maxIdleTime)
                {
                    _parts.AI.TriggerIdleFinish();
                }
                else
                {
                    _inIdleTime += Time.deltaTime;
                }
            }
        }

        public override void FixedUpdate()
        {

        }


        public override void OnStateExit()
        {

        }

    }
}