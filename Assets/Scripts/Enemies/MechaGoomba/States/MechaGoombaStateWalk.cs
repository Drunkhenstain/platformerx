﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    public class MechaGoombaStateWalk : MechaGoombaBaseState
    {
        private float _moveSpeed;
        public override void ApplySettings()
        {
            StateName = "Walk";
            _moveSpeed = _parts.Settings.MoveSpeed;
        }
        public override void OnStateEnter()
        {
            base.OnStateEnter();
            _parts.Anim.Play("Walk");
        }

        protected override bool OnCheckEntryConditions(MechaGoombaParts parts, MechaGoombaStateController machine)
        {
            return true;
        }
        public override void CheckExitConditions()
        {
        }

        public override void Update()
        {
            if (ExecuteStateLogic)
            {
            }
        }

        public override void FixedUpdate()
        {
            if (ExecuteStateLogic)
            { 
                _parts.Rigid.velocity = new Vector2(_parts.AI.InputX * _moveSpeed, _parts.Rigid.velocity.y);
            }
        }


        public override void OnStateExit()
        {

        }

    }
}