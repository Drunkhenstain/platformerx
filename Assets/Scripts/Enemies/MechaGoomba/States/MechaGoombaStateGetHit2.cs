﻿ 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    public class MechaGoombaStateGetHit2 : MechaGoombaBaseState
    {
        [SerializeField]
        private float _maxHitTime;

        private float _inHurtTime;

        public override void OnStateEnter()
        {
            base.OnStateEnter();
            _parts.Anim.speed = 1 / _maxHitTime;
            _parts.Anim.Play(StateName);
            _inHurtTime = 0;
        }
        public override void ApplySettings()
        {
            StateName = "GetHit2";
            //_maxHitTime = 0.5f;// _parts.Settings.HurtTime;
        }

        protected override bool OnCheckEntryConditions(MechaGoombaParts parts, MechaGoombaStateController machine)
        {
            return true;
        }

        public override void CheckExitConditions()
        {
        }

        public override void Update()
        {
            if (ExecuteStateLogic)
            {
                if (_inHurtTime >= _maxHitTime)
                {
                    _parts.AI.TriggerHitFinish();
                }
                else
                {
                    _inHurtTime += Time.deltaTime;
                }
            }
        }

        public override void FixedUpdate()
        {

        }


        public override void OnStateExit()
        {
            _parts.Anim.speed = 1;
        }

    }
}