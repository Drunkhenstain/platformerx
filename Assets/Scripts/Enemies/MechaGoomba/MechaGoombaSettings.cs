﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    public class MechaGoombaSettings : MonoBehaviour
    {
        [ColorUsage(true, true)]
        public Color32 GoombaColor;

        public LayerMask PlayerLayer;
        public LayerMask GroundLayer;
        public LayerMask SlippyGroundLayer;
        public LayerMask DeadlyGroundLayer;

        public float Health;
        public float PointsOnKill;

        public float MoveSpeed;

        public Vector2 PushPlayerOnAttackHitVelocity;
        public Vector2 PushPlayerOnTouchVelocity;
        public Vector2 PushPlayerOnTopJumpVelocity;

        public bool CanLookBack;

        public float LookDistanceX;

        public float AttackExtraXDistance;
        public float TouchDamage;
        public float HitDamage;

        [Range(0.01f, 10f)]
        public float IdleTime;

        [Range(0.01f, 10f)]
        public float HurtTime;

        [Range(0.01f,10f)]
        public float AttackTime;
    }
}

