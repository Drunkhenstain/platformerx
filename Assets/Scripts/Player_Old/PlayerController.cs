﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    [Header("Set in Editor")]
    public SpriteRenderer _playerSprite;
    public Rigidbody2D _rigid;
    public Animator _anim;
    public Transform _wallCheckLeftPos;
    public Transform _wallCheckRightPos;
    public Collider2D _baseCollider;

    //public Transform _groundCheckLeftPos;
    //public Transform _groundCheckRightPos;

    [SerializeField]
    private float _jumpBreakFactor = 0.5f;

    [SerializeField]
    private float _runBreakFactor = 0.5f;

    [SerializeField]
    private float _groundMoveSpeed = 200.0f;

    [SerializeField]
    private float _airMoveSpeed = 210.0f;

    [SerializeField]
    private float _airMoveTime = 2f;

    [SerializeField]
    private float _groundedDistanceY = 0.1f;

    [SerializeField]
    private float _maxFallingSpeed = 5f;


    [SerializeField]
    private LayerMask _groundLayer;


    [SerializeField]
    private bool _canJump;
    [SerializeField]
    private bool _isJumping;
    [SerializeField]
    private bool _isGrounded;
    [SerializeField]
    private bool _isFalling;
    [SerializeField]
    private bool _isOnWallLeft;
    [SerializeField]
    private bool _isOnWallRight;
    [SerializeField]
    private bool _isJumpHanging;
    [SerializeField]
    private bool _isWallSliding;


    [SerializeField]
    private float _jumpForce = 5.0f;
    [SerializeField]
    private float _jumpHangTime = 0.050f;
    [SerializeField]
    private float _jumpBtnBuffer = 0.2f;

    [SerializeField]
    private float _wallSlideVelocity = 2f;

    [SerializeField]
    private float _inputX;
    private float _inputXRaw;
    private bool _jumpBtnKeydown;
    private bool _jumpBtnKeyup;
    private bool _jumpBtnKeyPressed;


    private float _curJumpHangTime;
    private float _curAirMoveTime;
    private float _curJumpBtnBuffer;



    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        // Moving in FixedUpdate
        GetInputs();
        CheckGrounded();
        CheckFalling();

    }

    void FixedUpdate()
    {
        ProcessJump();
        ProcessWallSlide();

        if (_isWallSliding == false)
        {
            if (_isGrounded == true)
            {
                MoveOnGround();
            }
            else
            {
                MoveInAir();
            }
        }
    }

    void LateUpdate()
    {
        FlipSprites();
        _anim.SetBool("IsWallSliding", _isWallSliding);
        _anim.SetBool("IsJumping", _isJumping);
        _anim.SetBool("IsFalling", _isFalling);
        _anim.SetFloat("Move", Mathf.Abs(_inputX));
    }
     
    private void GetInputs()
    {
        _inputX = Input.GetAxis("Horizontal");
        _inputXRaw = Input.GetAxisRaw("Horizontal");

        _jumpBtnKeydown = Input.GetButtonDown("Jump");
        _jumpBtnKeyup = Input.GetButtonUp("Jump");
        _jumpBtnKeyPressed = Input.GetButton("Jump");

        if (_jumpBtnKeydown)
        {
            _curJumpBtnBuffer = _jumpBtnBuffer;
        }

        if (_curJumpBtnBuffer > 0)
        {
            _curJumpBtnBuffer -= Time.deltaTime;
            _jumpBtnKeydown = true;
        }
    }

    private void CheckGrounded()
    {
        if (Physics2D.OverlapCircle(_wallCheckLeftPos.position, _groundedDistanceY, _groundLayer))
        {
            _isOnWallLeft = true;
        }
        else
        {
            _isOnWallLeft = false;
        }
        if (Physics2D.OverlapCircle(_wallCheckRightPos.position, _groundedDistanceY, _groundLayer))
        {
            _isOnWallRight = true;
        }
        else
        {
            _isOnWallRight = false;
        }

        if (Physics2D.BoxCast(_baseCollider.bounds.center, _baseCollider.bounds.size, 0f, Vector2.down, _groundedDistanceY, _groundLayer.value))
        {
            _isGrounded = true;
        }
        else
        {
            _isGrounded = false;
        }
    }

    private void CheckFalling()
    {
        if (_isGrounded == false && 0 > _rigid.velocity.y)
        {
            _isFalling = true;
        }
        else if (_isGrounded == true && _rigid.velocity.y >= 0)
        {
            _isFalling = false;
        }

        if (_isFalling && _rigid.velocity.y < -_maxFallingSpeed)
        {
            _rigid.velocity = new Vector2(_rigid.velocity.x, -_maxFallingSpeed);
        }
    }

    private void MoveOnGround()
    {
        if (Mathf.Abs(_inputX) > 0.09f)
        {
            _rigid.velocity = new Vector2(_inputX * _groundMoveSpeed * Time.fixedDeltaTime, _rigid.velocity.y);
        }
        else
        {
            _rigid.velocity = new Vector2(_rigid.velocity.x * _runBreakFactor, _rigid.velocity.y);
        }
    }

    private void MoveInAir()
    {
        if (_inputX != 0 && _curAirMoveTime > 0 && _isJumping == true)
        {
            _rigid.velocity = new Vector2(_inputX * _airMoveSpeed * Time.fixedDeltaTime, _rigid.velocity.y);
            _curAirMoveTime -= Time.fixedDeltaTime;
        }
    }

    private void ProcessWallSlide()
    {
        if (_inputXRaw != 0 && _isFalling && (_isOnWallRight || _isOnWallLeft))
        {
            if (_inputXRaw < 0 && _isOnWallLeft)
            {
                _isWallSliding = true;
            }
            else if (_inputXRaw > 0 && _isOnWallRight)
            {
                _isWallSliding = true;
            }
            else
            {
                _isWallSliding = false;
            }
        }
        else
        {
            _isWallSliding = false;
        }

        if (_isWallSliding)
        {
            _rigid.velocity = new Vector2(_rigid.velocity.x, -_wallSlideVelocity);
        }
    }

    void ProcessJump()
    {
        if (_isGrounded || _isWallSliding)
        {
            _curJumpHangTime = _jumpHangTime;
            _curAirMoveTime = _airMoveTime;
        }
        else if (_curJumpHangTime > 0)
        {
            _curJumpHangTime -= Time.deltaTime;
        }

        if (_isJumpHanging == true && _curJumpHangTime <= 0)
        {
            _isJumpHanging = false;
        }
        else if (_curJumpHangTime > 0)
        {
            _isJumpHanging = true;
        }

        if (_isJumpHanging)
        {
            _canJump = true;
        }
        else
        {
            _canJump = false;
        }

        if (_isGrounded || _isWallSliding)
        {
            _isJumping = false;
        }

        if (_jumpBtnKeydown && _canJump == true)
        {
            // reset Y Velocity to make jump predictable
            _rigid.velocity = new Vector2(_rigid.velocity.x, 0);

            if (_isWallSliding)
            {
                if (_inputX > 0)
                {
                    _rigid.velocity = new Vector2(-55, 55);
                }
                else if (_inputX < 0)
                {
                    _rigid.velocity = new Vector2(55, 55);
                }
            }
            else
            {
                _rigid.AddForce(new Vector2(0, _jumpForce), ForceMode2D.Impulse);
            }

            _curJumpHangTime = 0;
            _curJumpBtnBuffer = 0;
            _canJump = false;
            _isJumping = true;
        }
        else if (_isJumping && _jumpBtnKeyPressed == false && _rigid.velocity.y > 0)
        {
            _rigid.velocity = new Vector2(_rigid.velocity.x, _rigid.velocity.y * _jumpBreakFactor);
        }

    }

    void FlipSprites()
    {
        if (_inputX > 0)
        {
            _playerSprite.flipX = false;
        }
        else if (_inputX < 0)
        {
            _playerSprite.flipX = true;
        }

        if (_isWallSliding)
        {
            _playerSprite.flipX = !_playerSprite.flipX;
        }
    }
}
