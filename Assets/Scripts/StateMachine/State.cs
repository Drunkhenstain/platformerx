﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    [RequireComponent(typeof(StateMachine))]
    public abstract class State : MonoBehaviour
    {
        public string GetName { get { return StateName; } }

        protected string StateName;

        public bool DrawStateGizmoz;
        public bool ExecuteStateLogic;
        protected StateMachine Machine;
        
        // OnValidate calls whenever you change something in the inspector
        void OnValidate()
        {
            Machine = GetComponent<StateMachine>();
        }


        public static implicit operator bool(State state)
        {
            return state != null;
        }

        // called by statemachine
        public void Initialize(StateMachine machine)
        {
            Machine = machine;
            OnStateInitialize();
        }

        // called by statemachine
        public void StateEnter()
        {
            enabled = true;
            ExecuteStateLogic = true;
            OnStateEnter();

            //Debug.Log(Time.frameCount + StateName);
        }
        
        // called by statemachine
        public void StateExit()
        {
            OnStateExit();
            enabled = false;
            ExecuteStateLogic = false;
        }

        // call base here and customize!
        public virtual void Update()
        {
            CheckExitConditions();
        }

        // customize!
        public abstract void ApplySettings();

        // customize!
        public abstract void OnStateInitialize();

        // customize!
        public abstract void OnStateEnter();

        // customize!
        public abstract void OnStateExit();

        // customize!
        public abstract void CheckExitConditions();

        // customize!
        public abstract void FixedUpdate();

        //public void OnEnable()
        //{
        //    if (this != Machine.GetCurrentState)
        //    {
        //        enabled = false;
        //        //Debug.LogWarning("Do not enable States directly. Use StateMachine.SetState");
        //    }
        //}

        //public void OnDisable()
        //{
        //    if (this == Machine.GetCurrentState)
        //    {
        //        enabled = true;
        //        //Debug.LogWarning("Do not disable States directly. Use StateMachine.SetState");
        //    }
        //}
    }
}
