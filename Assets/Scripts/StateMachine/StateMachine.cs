﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerX
{
    //https://noahbannister.blog/2017/12/19/building-a-modular-character-controller/
    public class StateMachine : MonoBehaviour
    {
        public bool LogStates;

        public State StartingState;
        protected List<State> statesList = new List<State>();

        [SerializeField]
        protected State lastState;
        [SerializeField]
        protected State currentState;

        public void SetCurrentStateLogic(bool execute)
        {
            if (currentState != null)
            {
                currentState.ExecuteStateLogic = execute;
            }
        }

        public void Start()
        {
            Init();
            //SetTransitions();
            SetState(StartingState);
        }

        public State GetCurrentState { get { return currentState; } }
        public State GetLastState { get { return lastState; } }

        /// <summary>
        /// Switch the currentState to a specific State object
        /// </summary>
        /// <param name="state">
        /// The state object to set as the currentState</param>
        /// <returns>Whether the state was changed</returns>
        public virtual bool SetState(State state)
        {
            bool success = false;
            if (state && state != currentState)
            {
                lastState = currentState;
                currentState = state;
                if (lastState)
                    lastState.StateExit();
                currentState.StateEnter();
                success = true;

                if (LogStates)
                {
                    Debug.Log(Time.frameCount + currentState.GetName);
                }
            }
            return success;
        }

        public void setState(State state)
        {
            SetState(state);
        }

        /// <summary>
        /// Switch the currentState to a State of a the given type.
        /// </summary>
        /// <typeparam name="StateType">
        /// The type of state to use for the currentState</typeparam>
        /// <returns>Whether the state was changed</returns>
        public virtual bool SetState<StateType>() where StateType : State
        {
            bool success = false;
            //if the state can be found in the list of states 
            //already created, switch to the existing version
            foreach (State state in statesList)
            {
                if (state is StateType)
                {
                    success = SetState(state);
                    return success;
                }
            }
            //if the state is not found in the list,
            //see if it is on the gameobject.
            State stateComponent = GetComponent<StateType>();
            if (stateComponent)
            {
                stateComponent.Initialize(this);
                statesList.Add(stateComponent);
                success = SetState(stateComponent);
                return success;
            }
            //if it is not on the gameobject,
            //make a new instance
            State newState = gameObject.AddComponent<StateType>();
            newState.Initialize(this);
            statesList.Add(newState);
            success = SetState(newState);

            return success;
        }

        public State GetState<StateType>() where StateType : State
        {
            //if the state can be found in the list of states 
            //already created, switch to the existing version
            foreach (State state in statesList)
            {
                if (state is StateType)
                {
                    return state;
                }
            }
            //if the state is not found in the list,
            //see if it is on the gameobject.
            State stateComponent = GetComponent<StateType>();
            if (stateComponent)
            {
                stateComponent.Initialize(this);
                statesList.Add(stateComponent);
                return stateComponent;
            }

            //if it is not on the gameobject, return null because we dont want to add it here
            return null;
        }

        // gets called in Start
        protected virtual void Init() { }
    }
}